# Suivi Aides Élèves Écoles


## Présentation

Cette application en ligne nommée A.S.D.A.E.L (Application Suivi Des Aides Élèves Lézignan) doit permettre de tracer/lister les différentes actions mises en place tout au long de leur scolarité dans le premier degré pour aider des élèves en difficulté. Elle vise à aider les équipes pédagogiques à avoir de façon centralisée et synthétique les actions mises en place et de définir des priorités. Elle aura son utilité lors des conseils de cycle ou de maîtres et maîtresses afin de déterminer collégialement les aides à mettre en place.

Les données contenues étant sensibles car s'agissant d'élèves, il faut veiller à leur sécurisation et à en limiter l'accès aux seul⋅es enseignant⋅es.

Elle est actuellement en cours de développement et devrait être testée sur le réseau local d'une école (pas d'accès aux données de l'extérieur).

À côté de l'application en ligne (vraiment intéressante pour les grosses écoles), vous trouverez aussi un classeur classique intitulé :  
* [Classeur_Parcours_Aides_Élèves_v3.ods](Classeur_Parcours_Aides_Élèves_v3.ods)

permettant la même chose mais sans avoir besoin d'un serveur. On pourra l'utiliser via Nuage-Nextcloud en mode "drive" pour pouvoir bénéficier des macros. Cette solution est plus simple à mettre en oeuvre (surtout si on n'a pas besoin de synchroniser les données et que les enseignant⋅es sont peu nombreux⋅euses) et plus adaptée aux petites structures.


## Utilisation de SQLPage

Ce projet s'appuie sur l'outil SQLPage développé, partagé et documenté par Ophir Lojkine (un grand merci à lui) permettant de construire des applications WEB pour gérer des bases de données de différents types (SQLite, PostGreSQL, MySQL, ...). SQLPage est un outil extrèmement intéressant et puissant pour développer rapidement des applications sans avoir à saisir autre chose que des commandes SQL. Plus d'info sur cette page : https://sql.ophir.dev.
Le choix s'est porté particulièrement sur SQLPage du fait que cette application intègre un accès sécurisé aux données avec un code d'activation des comptes par l'administrateur⋅rice et les directeur⋅rices afin que les utilisateur⋅rices puissent choisir leur propre mot de passe. Il s'agissait là d'un élèment fondamental, avant même de commencer à développer.

Ce projet a été largement inspiré par le travail de David Soulié, enseignant lui aussi, via son application École Inclusive visant à gérer les aides mises en place dans un PIAL/PAS qu'on peut trouver sur la Forge aussi : https://forge.apps.education.fr/dsoulie/Ecole_inclusive.

Pour découvrir les possibilités offerte par SQLPage vous pouvez aussi consulter ces 2 articles en ligne :
- présentation par l'auteur lui-même : https://linuxfr.org/news/ecrire-une-appli-web-en-une-journee-avec-sqlpage
- un retour d'expérience par un enseignant ayant développé l'application "École Inclusive" à partir de SQLPage : https://linuxfr.org/news/ecole-inclusive-une-application-libre-pour-la-prise-en-charge-des-eleves-en-situation-de-handicap


## Installation

Pour l'instant, je développe cette application via un NAS Synology personnel avec une image Docker de SQLPage : https://hub.docker.com/r/lovasoa/sqlpage. Il existe plusieurs images (la version "latest" qu'on récupère par défaut est moins avancée que la "main").

Pour savoir comment installer SQLPage sur un NAS Synology, voir cette page : https://lofurol.fr/joomla/logiciels-libres/104-bases-de-donnees/349-sqlpage-utilisation-sur-un-nas-synology-avec-docker-et-mysql-postgresql-sqlite.

La base fictive utilisée dans cette application est de type SQLite car elle ne nécessite pas la mise en place d'un autre serveur pour gérer les bases de données.


## Fonctionnement

- Seul⋅es les enseignant⋅es ayant un compte utilisateur⋅rice activé par l'admin peuvent se connecter à l'application. Sans connexion aucun accès aux informations.
- Les utilisateur⋅rices doivent activer leur compte à la première utilisation pour choisir leur mot de passe.
- L'application peut gérer plusieurs écoles différentes, ce qui peut être intéressant au niveau d'une ville, voire d'une circonscription à condition qu'elle soit hébergée sur un serveur institutionnel pour permettre d'y accèder par Internet(mais il faudrait voir la charge sur le serveur, sachant aussi qu'en terme de résilience ou de problème comme une fuite de données, plus on sépare les données mieux c'est).
- Il existe 4 types d'utilisateur⋅rices avec des droits d'accès différents :
    - **groupe=1 : RASED**
        - Pas de droit en écriture, consultation uniquement.
        - Seul⋅es les élèves suivi⋅es dans toutes les écoles sont visibles.
        - Accès aux besoins et parcours des élèves.
    - **groupe=2 : ENSEIGNANT⋅E**
        - Uniquement accès aux élèves de sa classe.
        - Droit de modification sur ses élèves.
        - Renseigne, modifie les besoins et parcours de ses élèves.
    - **groupe=3 : DIRECTEUR⋅RICE**
        - Accès aux élèves de son école uniquement.
        - Droit de modification sur tous les élèves de son école.
        - Renseigne, modifie les besoins et parcours des élèves.
        - Gère les comptes des enseignant⋅es de son école.
    - **groupe=4 : ADMINISTRATION**
        - A tous les droits.
        - Création et gestion de tous les comptes utilisateur⋅rices ayant le droit de se connecter.
- En début d'année scolaire, l'administrateur⋅rice importe les élèves de toute l'école via un fichier .csv issu de l'application ONDE. Attention, le fichier obtenu doit être retravaillé avant de réaliser l'importation (renommage des en-têtes de colonnes puis ré-encodage en UTF8). Ça peut se faire via macro dans LibreOffice Calc avec le fichier suivant "[Conversion_CSV_Extraction_ONDE.ods](Conversion_CSV_Extraction_ONDE.ods)".
- Les directeur⋅rices ne peuvent pas créer de comptes utilisateur⋅rices comme les administrateur⋅rices. Par contre, iels peuvent créer des comptes d'enseignant⋅es pour leur école, ce qui crée automatiquement le compte utilisateur⋅rice correspondant. Iels ont aussi accès aux codes d'activation à transmettre aux nouveaux comptes.


## Schéma de la base de données
![Schéma de la base de données](/Images/Schéma_Base_Données.png "Schéma de la base de données")

## Captures d'écran de l'application en ligne
Ci-dessous, captures d'écran de l'application en ligne :

![Page de connexion](/Images/Capture_connexion.png "Page de connexion")

![Page des établissements](/Images/Capture_établissements.png "Page des établissements")

![Liste des élèves suivie⋅es](/Images/Capture_élèves_suivi⋅es.png "Liste des élèves suivi⋅es")

![Page du suivi des aides d'un⋅e élève](/Images/Capture_élève_aides.png "Page du suivi des aides d'un⋅e élève")

![Page de paramétrage](/Images/Capture_Paramétrages.png "Page de paramétrage")

![Fiche de synthèse](/Images/Capture_fiche_synthétique.png 'Fiche synthétique des aides')


## Captures d'écran du classeur LibreOffice Calc
Ci-dessous, captures d'écran du classeur :

![Feuille Notice](/Images/Classeur_Notice.png "Feuille Notice")

![Feuille Aides Élève](/Images/Classeur_Fiche.png "Feuille de suivi des aides d'un élève")

![Feuille Liste](/Images/Classeur_Liste.png "Feuille listant les élèves suivi⋅es ayant eu des aides")


## Remarques
- La connexion CAS via l'ENT par exemple n'est pas fonctionnelle en l'état (les fichiers sont là mais non actifs).

- À faire :
    - ~~Attendre la prochaine montée de version de SQLPage 0.22 pour :~~
        - ~~Import et export .csv avec point-virgules (correction bug)~~
        - ~~Infobulles sur bouton~~

    - Mise à jour des données des élèves via une nouvelle importation d'une extraction CSV de ONDE en se basant sur l'INE. Utile lors du changement d'année scolaire (sans impact sur le parcours).

    - ~~**PRIORITAIRE** : Page synthétisant les aides mises en place de la TPS au CM2 avec commentaires en infobulles. Comme on ne peut pas anticiper les années de redoublement, il ne faudra le parcours que de la TPS au niveau suivi par l'élève pour l'année scolaire en cours.   
    Ci-dessous, voilà vers quoi on voudrait tendre :~~

    - Prévoir la possibilité d'importer des comptes enseignant⋅es par les directeur⋅rices

    - Exportation des données pour les ré-intégrer facilement.

    - Générer une fiche de suivi d'un⋅e élève au format pdf afin de pouvoir l'imprimer ou la transmettre.
