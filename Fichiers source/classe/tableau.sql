SELECT 'redirect' AS component,
        'signin.sql?error' AS link
 WHERE NOT EXISTS (SELECT 1 FROM login_session WHERE id=sqlpage.cookie('session'));
SET group_id = (SELECT user_info.groupe FROM login_session join user_info on user_info.username=login_session.username WHERE id = sqlpage.cookie('session'));

-- Calcul des variables établissement
SET NB_eleve = (SELECT count(distinct eleve.id) FROM eleve where eleve.etab_id=$id and eleve.classe=$classe_select);
SET NB_suivi = (SELECT count(distinct eleve.id) FROM eleve where eleve.etab_id=$id and eleve.classe=$classe_select and eleve.suivi=1);

-- écrire les infos de l'établissement dans le titre de la page [GRILLE]
SELECT 
    'datagrid' as component;
SELECT 
    ' Élèves suivi⋅es : ' as title,
    $NB_suivi as description,
    'users-plus' as icon;
SELECT 
    ' Élèves en tout : ' as title,
    $NB_eleve as description,
    TRUE           as active,
    'briefcase' as icon;

