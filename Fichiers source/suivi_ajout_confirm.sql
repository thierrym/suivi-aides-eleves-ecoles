SELECT 'redirect' AS component,
        'signin.sql?error' AS link
        WHERE NOT EXISTS (SELECT 1 FROM login_session WHERE id=sqlpage.cookie('session'));

SET group_id = (SELECT user_info.groupe FROM login_session join user_info on user_info.username=login_session.username WHERE id = sqlpage.cookie('session'));

SELECT 'redirect' AS component,
        'index.sql?id='||$id||'&restriction' AS link
        WHERE $group_id<'2';


UPDATE eleve SET modification=$horodatage, editeur=$edition WHERE id=$eleve_id;

-- ajouter une action de suivi à l'élève
INSERT INTO parcours(annee_id, eleve_id, etab_id, niveau, classe, enseignant_id, action, action_categorie, action_libelle, action_ordre, action_commentaire, horodatage)
        SELECT $annee_id, $eleve_id, $etab_id, $niveau, $classe, $enseignant_id, $action, $action_categorie, $action_libelle, $action_ordre, $action_commentaire, $horodatage
        RETURNING 'redirect' AS component,
            'suivi_eleve.sql?id='||$eleve_id as link;


--SELECT 'debug' as component, $eleve_id as eleve_id, $besoin1 as besoin1, $besoin2 as besoin2, $horodatage as date, $edition as editeur;
