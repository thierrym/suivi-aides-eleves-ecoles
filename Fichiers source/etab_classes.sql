SELECT 'redirect' AS component,
        'signin.sql?error' AS link
 WHERE NOT EXISTS (SELECT 1 FROM login_session WHERE id=sqlpage.cookie('session'));
SET group_id = (SELECT user_info.groupe FROM login_session join user_info on user_info.username=login_session.username WHERE id = sqlpage.cookie('session'));

SELECT 'redirect' AS component,
        'etablissement.sql?restriction' AS link
FROM eleve WHERE (SELECT user_info.etab FROM login_session join user_info on user_info.username=login_session.username WHERE id = sqlpage.cookie('session') and user_info.etab<>$id);

--Menu
SELECT 'dynamic' AS component, 
CASE WHEN $group_id=1
THEN sqlpage.read_file_as_text('index.json')
ELSE sqlpage.read_file_as_text('menu.json')
            END    AS properties; 

--Bouton retour à la liste des établissements
select 
    'button' as component,
    'sm'     as size,
    'pill'   as shape;
select 
    'Retour à la liste des établissements' as title,
    'etablissement.sql' as link,
    'arrow-back-up' as icon,
    'green' as outline;


-- Sous-menu / bascule
SELECT 
    'button' as component,
    'sm'     as size,
    --'pill'   as shape,
    'center' as justify;
select 
    'Classes' as title,
    'users-group' as icon,    
    'orange' as color;
select 
    'Carte' as title,
    'etab_carte.sql?id=' || $id as link,
    'map' as icon,
    'teal' as outline;
select 
    'Stats' as title,
    'etab_stats.sql?id=' || $id as link,
    'chart-histogram' as icon,
    'teal' as outline;
select 
    'Photos' as title,
    'etab_trombi.sql?id=' || $id as link,
    'camera' as icon,
    'teal' as outline;

-- Récupère les nombres d'élèves dans 2 variables 
SET NB_eleve = (SELECT count(distinct eleve.id) FROM eleve where eleve.etab_id=$id);
SET NB_suivi = (SELECT count(distinct eleve.id) FROM eleve where eleve.etab_id=$id AND eleve.suivi=1);

-- écrire les infos de l'établissement dans le titre de la page [GRILLE]
SELECT 
    'datagrid' as component,
    type || ' ' || nom_etab as title FROM etab WHERE id = $id;
    select 
        ' Élèves suivi⋅es : ' as title,
        $NB_suivi ||' sur ' || $NB_eleve as description,
        'briefcase' as icon;


-- Sous-menu / classes
SET classe_etab = (SELECT classe FROM eleve INNER JOIN etab on eleve.etab_id = etab.id WHERE etab.id=$id and eleve.suivi=1)
SET classe_select = coalesce($Classe, coalesce($classe_select, $classe_etab));
/*
select 
    'button' as component,
    'sm'     as size,
    'pill'   as shape;
select 
    CASE WHEN $classe_select is Null THEN 'sélectionner une classe'
    ELSE 'Classe : ' || $classe_select 
    END as title,
    'users-group' as icon,
    'red' as outline;
select 
    eleve.classe as title,
    'etab_classes.sql?id=' || etab.id || '&classe_select=' || eleve.classe as link,
    'users-group' as icon,
    'green' as outline
    FROM etab INNER JOIN eleve on eleve.etab_id=etab.id where etab.id=$id GROUP BY eleve.classe ORDER BY eleve.classe ASC;
*/ 

/*   
--alternative
 SELECT 
    'form' as component,
    'etab_classes.sql?id=' || $id || '&classe_select=' ||$Classe AS action,
    'Valider' as validate,
    'green'           as validate_color;   
     SELECT 'Classe' AS name, 'select' as type, 3 as width, $classe_select as value, json_group_array(json_object('label', classe, 'value', classe)) as options FROM (select distinct eleve.classe as classe, eleve.classe as value FROM eleve JOIN etab on eleve.etab_id=etab.id WHERE eleve.etab_id=$id  ORDER BY eleve.classe DESC);

-- Calcul des variables établissement
SET NB_eleve = (SELECT count(distinct eleve.id) FROM eleve where eleve.etab_id=$id and eleve.classe=$classe_select);
-- Personnalisation NB_accomp pour version classe :
SET NB_accomp = (SELECT count(distinct suivi.eleve_id) FROM suivi JOIN eleve on suivi.eleve_id=eleve.id WHERE suivi.aesh_id<>1 and eleve.etab_id=$id and eleve.classe=$classe_select);
SET NB_notif = (SELECT count(notification.id) FROM notification JOIN eleve on notification.eleve_id = eleve.id WHERE eleve.etab_id = $id and eleve.classe=$classe_select);
SET NB_aesh = (SELECT count(distinct suivi.aesh_id) FROM suivi JOIN eleve on suivi.eleve_id=eleve.id WHERE eleve.etab_id=$id and eleve.classe=$classe_select and suivi.aesh_id<>1);

-- écrire les infos de l'établissement dans le titre de la page [GRILLE]
SELECT 
    'datagrid' as component,
    type || ' ' || nom_etab ||' --- Classe : ' || $classe_select as title FROM etab WHERE id = $id;
SELECT 
    ' Élèves accompagnés : ' as title,
    $NB_accomp as description,
    'users-plus' as icon;
SELECT 
    ' Élèves à suivre : ' as title,
    $NB_eleve as description,
    TRUE           as active,
    'briefcase' as icon;
SELECT 
' AESH ' as title,
    $NB_aesh as description,
    'user-plus' as icon;
*/    
-- En-tête
select 
    'card' as component,
     2      as columns;
select 
    '/classe/choix.sql?_sqlpage_embed'|| '&classe_select=' ||$classe_select||'&id=' || $id  as embed;
select 
    '/classe/tableau.sql?_sqlpage_embed'|| '&classe_select=' ||$classe_select||'&id=' || $id as embed;
  
SELECT 'title' as component,
    'Classe : ' || $classe_select as contents,
    TRUE as center,
    2 as level;

--Onglets
SET tab=coalesce($tab,'Résumé');
select 'tab' as component;

select  'Résumé' as title, 'list-check' as icon, 1 as active, 'etab_classes.sql?id='||$id||'&classe_select='|| $classe_select||'&tab=Résumé' as link, CASE WHEN $tab='Résumé' THEN 'orange' ELSE 'green' END as color;    
select  'Détails' as title, 'printer' as icon, 1 as active,'etab_classes.sql?id='||$id||'&classe_select='|| $classe_select||'&tab=Détails' as link, CASE WHEN $tab='Détails' THEN 'orange' ELSE 'green' END as color; 

-- Liste des élèves
SELECT 'table' as component,
    TRUE    as hover,
    TRUE    as small,
    'Actions' as markdown,
    'Suivi' as markdown,
    'Admin' as markdown,
    1 as sort,
    1 as search
    where $tab='Résumé';
    
SELECT 
    eleve.nom as Nom,
    eleve.prenom as Prénom,
    CASE WHEN $group_id>1 THEN
        '[
        ![](./icons/pencil.svg)
        ](eleve_edit.sql?id='||eleve.id||')'
        ELSE
        '[
        ![](./icons/pencil-off.svg)
        ]()'
        END as Admin,
     '[![](./icons/lifebuoy.svg)](eleve_besoins.sql?id='||eleve.id||' "Voir ses besoins")
    [![](./icons/writing.svg)](parcours.sql?id='||eleve.id||' "Voir les aides mises en place")'
    as Suivi

    FROM eleve INNER JOIN etab on eleve.etab_id = etab.id WHERE eleve.etab_id=$id and eleve.classe=$classe_select and eleve.suivi=1 and $tab='Résumé' GROUP BY eleve.id ORDER BY eleve.nom ASC;  
  
-- Télécharger les données
SELECT 
    'csv' as component,
    'Exporter les suivis de la classe '|| $classe_select as title,
    'Suivis'||$classe_select as filename,
    'file-download' as icon,
    'green' as color
        where $tab='Résumé';
SELECT 
    eleve.classe as Classe,
    eleve.nom as Nom,
    eleve.prenom as Prénom
  FROM eleve INNER JOIN etab on eleve.etab_id = etab.id WHERE eleve.etab_id=$id and eleve.classe=$classe_select and eleve.suivi=1 and $tab='Résumé' ORDER BY eleve.nom ASC;  

    
-- fiche détaillée des élèves suivis
-- Vers page simplifiée pour impression
select 
    'button' as component,
    'sm'     as size,
    --'pill'   as shape,
    'end' as justify
               where $tab='Détails';
select 
    'Visualiser pour impression' as title,
    'etab_classes_print.sql?id=' || $id ||'&classe_select='|| $classe_select as link,
    'printer' as icon,
    'green' as outline
               where $tab='Détails';

SELECT 'text' as component,
   'Fiches des élèves suivis' as title
           where $tab='Détails';

  SELECT 'table' as component, 
    TRUE    as hover,
    TRUE    as small
    where $tab='Détails';
    SELECT 
    eleve.nom||' '||eleve.prenom as élève
  FROM eleve LEFT JOIN etab on eleve.etab_id = etab.id WHERE eleve.etab_id=$id and eleve.classe=$classe_select and eleve.suivi=1 and $tab='Détails' ORDER BY eleve.nom ASC;  
  

