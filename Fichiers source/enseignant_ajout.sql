SELECT 'redirect' AS component,
        'signin.sql?error' AS link
 WHERE NOT EXISTS (SELECT 1 FROM login_session WHERE id=sqlpage.cookie('session'));
SET group_id = (SELECT user_info.groupe FROM login_session join user_info on user_info.username=login_session.username WHERE id = sqlpage.cookie('session'));
SELECT 'redirect' AS component,
        'parametres.sql?restriction' AS link
        WHERE $group_id<'3';

--Menu
SELECT 'dynamic' AS component, sqlpage.read_file_as_text('menu.json') AS properties;

--Sous-menu
SELECT
    'button' as component,
    'sm'     as size,
    'pill'   as shape,
    'center' as justify;
    select
        'Enseignant⋅e' as title,
        'enseignant.sql' as link,
        'writing' as icon,
        'Orange' as color,
        'orange' as outline;
    select
        'Établissements' as title,
        'etab.sql' as link,
        'building-community' as icon,
        'orange' as outline;
    select
        'Retour à la liste des enseignant⋅es' as title,
        'enseignant.sql' as link,
        'arrow-back-up' as icon,
        'green' as outline;

-- Récupère l'établissement auquel est rattaché⋅e la personne réalisant l'ajout (directeur⋅rice ou admin)
SET etab_user=(SELECT user_info.etab FROM login_session join user_info on user_info.username=login_session.username WHERE id = sqlpage.cookie('session'));

-- Génère aléatoirement le code d'activation'
SET validation=sqlpage.random_string(20);
SET validation_finale=$validation;

-- Saisir un nouvel enseignant
SELECT
    'form' as component,
    'Nouvel⋅le enseignant⋅e' as title,
    --intitulé du bouton de validation
    CASE WHEN $classe_ens is NULL and $etab_ens is NULL
            THEN 'Continuer'
        ELSE 'Créer un⋅e enseignant⋅e'
        END as validate,
    -- action du bouton de validation
    CASE WHEN $classe_ens is NULL and $etab_ens is NULL
            THEN ''
        ELSE 'create_enseignant.sql'
        END as action,
    -- couleur du bouton de validation
    CASE WHEN $classe_ens is NULL and $etab_ens is NULL
            THEN 'orange'
        ELSE 'green'
        END as validate_color;


    SELECT 'username' AS name, 'id' as prefix_icon, 'Identifiant' as label, $username as value, 6 as width, TRUE as required;
    SELECT 'activation' AS name, 'lock' as prefix_icon, 'text' AS type, $validation_finale AS value, 'Code d''activation' as label, 6 as width;
    SELECT 'nom' AS name, 'Nom' AS label, 'user' as prefix_icon, $nom as value, 6 as width, TRUE as required;
    SELECT 'prenom' AS name, 'Prénom' AS label, 'user' as prefix_icon, $prenom as value, 6 as width, TRUE as required;
    SELECT 'groupe' AS name, 'Permissions' as label, 'select' as type, 4 as width, TRUE as readonly, 2 as value,
    '[{"label": "Professeur⋅e", "value": 2}]' as options;
    SELECT 'etab_ens' AS name, 'Établissement' as label, TRUE as required, 'select' as type, CAST($etab_user as Integer) as value, 3 as width, json_group_array(json_object("label", nom_etab, "value", id)) as options
        FROM (select nom_etab, id FROM etab union all select 'Aucun' as label, NULL as value ORDER BY nom_etab ASC) where id=CAST($etab_user as Integer) having $etab_user is not null;
    SELECT 'etab_ens' AS name, 'Établissement' as label, TRUE as required, 'select' as type, CAST($etab_ens as Integer) as value, 3 as width, json_group_array(json_object("label", nom_etab, "value", id)) as options
        FROM (select nom_etab, id FROM etab union all select 'Aucun' as label, NULL as value ORDER BY nom_etab ASC) having $etab_user is null;

    SELECT 'classe_ens' AS name, 'Classe' as label, $classe_ens as value, 'select' as type, 2 as width, json_group_array(json_object("label", classe, "value", classe)) as options
        FROM (select vue_classes.classe, vue_classes.classe FROM vue_classes WHERE vue_classes.etab_id=CAST($etab_ens as Integer) union all select 'Aucune' as label, NULL as value ORDER BY vue_classes.classe ASC)
             WHERE CAST($groupe as Integer)=2 having $etab_ens is not null;
    SELECT 'tel_ens' AS name, 'Téléphone' AS label, $tel_ens as value, 'phone' as prefix_icon, CHAR(10), 4 as width;
    SELECT 'email' AS name, 'Courriel' AS label, 'mail' as prefix_icon, $email AS value,  4 as width;



SELECT 'table' as component, 'icon' as icon, 'nom_ens' as Nom, 'prenom_ens' as Prénom, 'tel_ens' as Téléphone, 'email' as courriel, 'etab_ens' as Établissement, 'classe_ens' as Classe, 'Actions' as markdown, 1 as sort, 1 as search;
SELECT nom_ens AS Nom, prenom_ens AS Prénom, tel_ens as Téléphone, email as courriel, etab.nom_etab as Établissement, classe_ens as Classe,
            CASE WHEN $group_id>2 THEN
            '[
            ![](./icons/pencil.svg)
            ](enseignant_edit.sql?id='||enseignant.id||'&username='||enseignant.username||') [
            ![](./icons/trash.svg)
            ](enseignant_delete.sql?id='||enseignant.id||')'
            ELSE
            '[
            ![](./icons/pencil-off.svg)]() [
            ![](./icons/trash-off.svg)]()'
            END as Actions
    FROM enseignant LEFT JOIN etab on enseignant.etab_ens=etab.id LEFT JOIN user_info on enseignant.username=user_info.username WHERE enseignant.etab_ens=$etab_user or $group_id=4 GROUP BY enseignant.id ORDER BY enseignant.nom_ens ASC;

--SELECT
--    'hero' as component,
--    '/enseignant_delete.sql' as link,
--    'Supprimer des enseignant⋅es' as link_text;

--Débogage
SELECT 'debug' as component, $etab_ens as etab_ens, $validation_finale as code;
