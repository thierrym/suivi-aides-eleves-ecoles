-- Liste de toustes les élèves suivi⋅es dans tous les établissements

SELECT 'redirect' AS component,
        'signin.sql?error' AS link
 WHERE NOT EXISTS (SELECT 1 FROM login_session WHERE id=sqlpage.cookie('session'));

SET group_id = (SELECT user_info.groupe FROM login_session join user_info on user_info.username=login_session.username WHERE id = sqlpage.cookie('session'));
SET group_bouton=$group_id;

--Menu
SELECT 'dynamic' AS component,
CASE WHEN $group_id=1
THEN sqlpage.read_file_as_text('index.json')
ELSE sqlpage.read_file_as_text('menu.json')
            END    AS properties;

-- Message si droits insuffisants sur une page
SELECT 'alert' as component,
    'Attention !' as title,
    'Vous ne possédez pas les droits suffisants pour accéder à cette page.' as description_md,
    'alert-circle' as icon,
    'red' as color
WHERE $restriction IS NOT NULL;

SELECT 'title' as component,
   'Liste des élèves suivi⋅es dans tous les établissements' as contents,
   TRUE as center,
   2 as level;

 select
    'text' as component,
   '##### Légende :
![Cartable](./icons/lifebuoy.svg) Besoins de l''élève /  ![Parcours](./icons/list.svg) Aides mises en place
' as contents_md;

-- Liste des élèves
SELECT 'table' as component,
    TRUE    as hover,
    TRUE    as small,
    'Info Aides' AS markdown,
    1 as sort,
    1 as search
    WHERE $group_id=1 OR $group_id=4;
SELECT
    eleve.nom as Nom,
    eleve.prenom as Prénom,
    strftime('%d/%m/%Y',eleve.naissance) AS Naissance,
    eleve.classe as Classe,
    etab.nom_etab as Établissement,
    --eleve.suivi as Suivi,
    '[![](./icons/lifebuoy.svg)](eleve_besoins.sql?id='||eleve.id||' "Voir les besoins")
    [![](./icons/list.svg)](suivi_eleve.sql?id='||eleve.id||' "Voir les aides mises en place")'
    as 'Info Aides'
FROM eleve INNER JOIN etab on eleve.etab_id=etab.id LEFT JOIN user_info join login_session on user_info.username=login_session.username WHERE login_session.id = sqlpage.cookie('session') AND eleve.suivi=1  GROUP BY eleve.id ORDER BY eleve.nom ASC;

SELECT 'table' as component,
    TRUE    as hover,
    TRUE    as small,
    1 as sort,
    1 as search
    WHERE $group_id>1 AND $group_id<4;
SELECT
    eleve.nom as Nom,
    eleve.prenom as Prénom,
    strftime('%d/%m/%Y',eleve.naissance) AS Naissance,
    eleve.classe as Classe,
    etab.nom_etab as Établissement,
    eleve.suivi as Suivi
FROM eleve INNER JOIN etab on eleve.etab_id=etab.id LEFT JOIN user_info join login_session on user_info.username=login_session.username WHERE user_info.etab=etab.id and login_session.id = sqlpage.cookie('session')  GROUP BY eleve.id ORDER BY eleve.nom ASC;
