-- Page listant tous les élèves dans la base de données

SELECT 'redirect' AS component,
        'signin.sql?error' AS link
 WHERE NOT EXISTS (SELECT 1 FROM login_session WHERE id=sqlpage.cookie('session'));

SET group_id = (SELECT user_info.groupe FROM login_session join user_info on user_info.username=login_session.username WHERE id = sqlpage.cookie('session'));
SET classe_prof = (SELECT user_info.classe FROM login_session join user_info on user_info.username=login_session.username WHERE id = sqlpage.cookie('session'));
SET etab_prof = (SELECT user_info.etab FROM login_session join user_info on user_info.username=login_session.username WHERE id = sqlpage.cookie('session'));

SELECT 'redirect' AS component,
        'etablissement.sql?restriction' AS link
        WHERE $group_id<'2';

--Menu
SELECT 'dynamic' AS component, sqlpage.read_file_as_text('menu.json') AS properties;

-- Message si droits insuffisants sur une page
SELECT 'alert' as component,
    'Attention !' as title,
    'Vous ne possédez pas les droits suffisants pour accéder à cette page.' as description_md,
    'alert-circle' as icon,
    'red' as color
WHERE $restriction IS NOT NULL;

-- Messages d'importation
SELECT 'alert' as component,
    'Mise à jour de la base :' as title,
    'l''opération s''est déroulée correctement.' as description_md,
    'alert-circle' as icon,
    'green' as color
WHERE $update=1;
SELECT 'alert' as component,
    'Importation dans la base :' as title,
    'l''opération s''est déroulée correctement.' as description_md,
    'alert-circle' as icon,
    'green' as color
WHERE $upload=1;

select
    'button' as component,
    'sm'     as size,
    'pill'   as shape;
select
    'Élèves' as title,
    'briefcase' as icon,
    'green' as color;
select
    'Ajouter un élève' as title,
    'eleve.sql' as link,
    'square-rounded-plus' as icon,
        $group_id<3 as disabled,
    'green' as outline;
select
    'Importer des élèves de ONDE' as title,
    'comptes_import_eleve.sql' as link,
    'square-rounded-plus' as icon,
        $group_id<3 as disabled,
    'green' as outline;

 select
    'text' as component,
   '##### Légende :
![](./icons/list.svg) Aides mises en place / ![](./icons/report.svg) Synthèse du suivi
' as contents_md;

-- Liste des élèves
SELECT 'table' as component,
    TRUE    as hover,
    TRUE    as small,
    'suivi' as markdown,
    'admin' AS markdown,
    1 as sort,
    1 as search;
    select
      eleve.nom as Nom,
      eleve.prenom as Prénom,
      strftime('%d/%m/%Y',eleve.naissance) AS Naissance,
      eleve.niveau as Niveau,
      eleve.classe as Classe,
      etab.nom_etab as Établissement,
      CASE
            WHEN eleve.suivi=1
                THEN '[
                    ![](./icons/list.svg "Voir le suivi")](suivi_eleve.sql?id='||eleve.id||')
                    [
                    ![](./icons/report.svg "Synthèse du suivi")](suivi_eleve_synthese.sql?id='||eleve.id||')'
            ELSE ''
            END as suivi,
        CASE
            WHEN $group_id=2 or $group_id=3 THEN
                '[
                ![](./icons/trash-off.svg)
                ]()[
                ![](./icons/pencil.svg "Modifier")
                ](eleve_edit.sql?id='||eleve.id||')'
            WHEN $group_id=4 THEN
                '[
                ![](./icons/trash.svg)
                ](eleve_delete.sql?id='||eleve.id||' "Supprimer")[
                ![](./icons/pencil.svg)
                ](eleve_edit.sql?id='||eleve.id||' "Modifier")'
            ELSE
                '[
                ![](./icons/trash-off.svg)
                ]()[
                ![](./icons/pencil-off.svg)
                ]()'
            END as admin
    FROM eleve INNER JOIN etab on eleve.etab_id=etab.id WHERE $group_id=4 OR $group_id=1 or (eleve.classe=$classe_prof and $group_id=2) or (eleve.etab_id=$etab_prof and $group_id=3) GROUP BY eleve.id ORDER BY eleve.nom ASC;

--SELECT 'debug' as component, $classe_prof as Classe, $etab_prof as Établissement, $group_id as Groupe;
