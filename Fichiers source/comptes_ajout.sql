SELECT 'redirect' AS component,
        'signin.sql?error' AS link
 WHERE NOT EXISTS (SELECT 1 FROM login_session WHERE id=sqlpage.cookie('session'));

SET group_id = (SELECT user_info.groupe FROM login_session join user_info on user_info.username=login_session.username WHERE id = sqlpage.cookie('session'));

SELECT 'redirect' AS component,
        'index.sql?restriction' AS link
        WHERE $group_id<>'4';

--Menu
SELECT 'dynamic' AS component, sqlpage.read_file_as_text('menu.json') AS properties;

--Bouton pour revenir à la liste des comptes
select
    'button' as component,
    'sm'     as size,
    'pill'   as shape;
select
    'Retour' as title,
    'comptes.sql' as link,
    'arrow-back-up' as icon,
    'green' as outline;

--Formulaire pour ajouter un compte
SELECT
    'form' as component,
    'Nouveau compte utilisateur' as title,
    --intitulé du bouton de validation
    CASE WHEN $groupe is null
            THEN 'Continuer'
        WHEN CAST($groupe as Integer)=2 and $classe is NULL
            THEN 'Continuer'
        WHEN CAST($groupe as Integer)=3 and $etab is NULL
            THEN 'Continuer'
        WHEN CAST($groupe as Integer)=1 or CAST($groupe as Integer)=4
            THEN 'Créer le nouveau compte'
        ELSE 'Créer le nouveau compte'
        END as validate,
    -- action du bouton de validation
    CASE WHEN $groupe is null
            THEN ''
        WHEN CAST($groupe as Integer)=2 and $classe is NULL
            THEN ''
        WHEN CAST($groupe as Integer)=3 and $etab is NULL
            THEN ''
        WHEN CAST($groupe as Integer)=1 or CAST($groupe as Integer)=4
            THEN 'create_user.sql'
        ELSE 'create_user.sql'
        END as action,
    -- couleur du bouton de validation
    CASE WHEN $groupe is null
            THEN 'orange'
        WHEN CAST($groupe as Integer)=2 and $classe is NULL
            THEN 'orange'
        WHEN CAST($groupe as Integer)=3 and $etab is NULL
            THEN 'orange'
        WHEN CAST($groupe as Integer)=1 or CAST($groupe as Integer)=4
            THEN 'green'
        ELSE 'green'
        END as validate_color;
    -- bouton de reset (enlevé car formulaire en cascade et n'agit pas sur le bouton de validation)
    --'Recommencer' as reset;


-- Formulaire
    select 'username' AS name, 'Identifiant' as label, $username as value, 6 as width, TRUE as required;
    select 'activation' AS name, 'text' AS type, sqlpage.random_string(20) AS value, 'Code d''activation' as label, 6 as width, TRUE as required;
    select 'nom' AS name, 'Nom' as label, $nom as value, 4 as width, TRUE as required;
    select 'prenom' AS name, 'Prénom' as label, $prenom as value, 4 as width, TRUE as required;
    select 'tel' AS name, 'Téléphone' AS label, $tel as value, 3 as width;
    select 'courriel' AS name, 'Courriel' AS label, $courriel as value, 3 as width;
    select 'groupe' AS name, 'Permissions' as label, 'select' as type, 12 as width, CAST($groupe as Integer) as value,
        '[{"label": "Consultation RASED", "value": 1}, {"label": "Enseignant⋅e", "value": 2}, {"label": "Directeur⋅rice", "value": 3}, {"label": "Administrateur", "value": 4}]' as options;
    --select 'Identifiant ENT' AS label, 'cas' AS name, 3 as width;

    select 'etab' AS name, 'Établissement' as label, 'select' as type, 2 as width, CAST($etab as Integer) as value,
            json_group_array(json_object("label", nom_etab, "value", id)) as options FROM (select nom_etab, id FROM etab union all select 'Aucun' as label, NULL as value ORDER BY nom_etab ASC)
            WHERE CAST($groupe as Integer)=2 or CAST($groupe as Integer)=3 having $groupe is not null AND (CAST($groupe as Integer)=2 or CAST($groupe as Integer)=3);

    select 'classe' AS name, 'Classe' as label, 'select' as type, 2 as width, $classe as value, json_group_array(json_object("label", classe, "value", classe)) as options
        FROM (select vue_classes.classe, vue_classes.classe FROM vue_classes WHERE vue_classes.etab_id=CAST($etab as Integer) union all select 'Aucune' as label, NULL as value ORDER BY vue_classes.classe ASC)
            WHERE CAST($groupe as Integer)=2 having $etab is not null AND CAST($groupe as Integer)=2;

SELECT 'debug' as component, $groupe as Groupe, CAST($etab as Integer) as Etab, $classe as Classe;
