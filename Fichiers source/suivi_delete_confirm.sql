SELECT 'redirect' AS component,
        'signin.sql?error' AS link
 WHERE NOT EXISTS (SELECT 1 FROM login_session WHERE id=sqlpage.cookie('session'));
SET group_id = (SELECT user_info.groupe FROM login_session join user_info on user_info.username=login_session.username WHERE id = sqlpage.cookie('session'));
SELECT 'redirect' AS component,
        'index.sql?restriction' AS link
        WHERE $group_id<'3';

DELETE FROM parcours
WHERE parcours.id = CAST($id as Integer) and $confirmed is not null
RETURNING
   'redirect' AS component,
   'suivi_eleve.sql?id='||$eleve_id as link;

select 'alert' as component, 'Suppression du suivi : ' || $id as title;
select 'Supprimer' as title, '?confirmed&id='||$id||'&eleve_id='||$eleve_id as link;
select 'Annuler' as title, 'suivi_eleve.sql?id='||$eleve_id as link;

--select 'debug' as component, $id as suivi, $eleve_id as eleve, $confirmed as confirmed;
