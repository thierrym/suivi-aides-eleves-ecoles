-- Page pour gérer les niveaux de classes

SELECT 'redirect' AS component,
        'signin.sql?error' AS link
 WHERE NOT EXISTS (SELECT 1 FROM login_session WHERE id=sqlpage.cookie('session'));
SET group_id = (SELECT user_info.groupe FROM login_session join user_info on user_info.username=login_session.username WHERE id = sqlpage.cookie('session'));

SELECT 'redirect' AS component,
        'index.sql?restriction' AS link
        WHERE $group_id<3;

--Insertion dans la base
 INSERT INTO niveaux(niv) 
    SELECT $niv WHERE $niv IS NOT NULL;

--Menu
SELECT 'dynamic' AS component, sqlpage.read_file_as_text('menu.json') AS properties;
  
--Sous-menu
select 
    'button' as component,
    'sm'     as size,
    'pill'   as shape,
    'center' as justify;
select 
    'Enseignant⋅e' as title,
    'enseignant.sql' as link,
    'writing' as icon,
    'Orange' as color,
    'orange' as outline;
select 
    'Établissements' as title,
    'etab.sql' as link,
    'building-community' as icon,
    'orange' as outline;
select 
    'Niveaux' as title,
    'stairs' as icon,
    'orange' as color;    
    

-- Liste et ajout
select 
    'card' as component,
     2      as columns;
select 
    '/niveaux/liste.sql?_sqlpage_embed' as embed;
