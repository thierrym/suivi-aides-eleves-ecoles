SELECT 'redirect' AS component,
        'signin.sql?error' AS link
 WHERE NOT EXISTS (SELECT 1 FROM login_session WHERE id=sqlpage.cookie('session'));
SET group_id = (SELECT user_info.groupe FROM login_session join user_info on user_info.username=login_session.username WHERE id = sqlpage.cookie('session'));
SELECT 'redirect' AS component,
        'index.sql?restriction' AS link
        WHERE $group_id<'2';

DELETE FROM fiche
WHERE fiche.id = CAST($id as Integer) and $confirmed is not null
RETURNING
   'redirect' AS component,
   'fiches.sql' as link;

select 'alert' as component, 'Suppression de la fiche : ' || $id as title;
select 'Supprimer' as title, '?confirmed&id='||$id as link;
select 'Annuler' as title, 'fiches.sql' as link;

--select 'debug' as component, $id as suivi, $eleve_id as eleve, $confirmed as confirmed;
