SELECT 'redirect' AS component,
        'signin.sql?error' AS link
 WHERE NOT EXISTS (SELECT 1 FROM login_session WHERE id=sqlpage.cookie('session'));
SET group_id = (SELECT user_info.groupe FROM login_session join user_info on user_info.username=login_session.username WHERE id = sqlpage.cookie('session'));

SELECT 'redirect' AS component,
        'index.sql?restriction' AS link
        WHERE $group_id<3;

--Insertion dans la base
 INSERT INTO annee(annee) 
 SELECT $an WHERE $an IS NOT NULL;

--Menu
SELECT 'dynamic' AS component, sqlpage.read_file_as_text('menu.json') AS properties;
  
--Sous-menu
select 
    'button' as component,
    'sm'     as size,
    'pill'   as shape,
    'center' as justify;
select 
    'Enseignant⋅e' as title,
    'enseignant.sql' as link,
    'writing' as icon,
    'Orange' as color,
    'orange' as outline,
    '![Cartable](./icons/briefcase.svg "test") Dossier de l''élève ' as contents_md;
select 
    'Établissements' as title,
    'etab.sql' as link,
    'building-community' as icon,
    'orange' as outline;
select 
    'Niveaux' as title,
    'niveaux.sql' as link,
    'stairs' as icon,
    'orange' as outline;    
select 
    'Années' as title,
    'calendar-month' as icon,
    'orange' as color;     

-- Liste et ajout
select 
    'card' as component,
     2      as columns;
select 
    '/annee/liste.sql?_sqlpage_embed' as embed;
select 
    '/annee/form.sql?_sqlpage_embed' as embed;
    
select 
    'button' as component,
    'sm'     as size,
    'pill'   as shape,
    'start' as justify; 
select 
    'Générer l''historique de l''année' as title,
    'tool_MAJ_parcours.sql' as link,
    'calendar-month' as icon,
    $group_id<4 as disabled,
    'orange' as outline;
