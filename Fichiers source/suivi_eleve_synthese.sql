-- Page permettant de lister toutes les actions du suivi d'un élève

-- Informations sur l'utilisateur⋅rice connecté
SET group_id = (SELECT user_info.groupe FROM login_session join user_info on user_info.username=login_session.username WHERE id = sqlpage.cookie('session'));
SET classe_prof = (SELECT user_info.classe FROM login_session join user_info on user_info.username=login_session.username WHERE id = sqlpage.cookie('session'));
SET etab_prof = (SELECT user_info.etab FROM login_session join user_info on user_info.username=login_session.username WHERE id = sqlpage.cookie('session'));

SELECT 'redirect' AS component,
        'index.sql?restriction' AS link
        WHERE $group_id<'1';

--Menu
/*SELECT 'dynamic' AS component,
    CASE WHEN $group_id=1
        THEN sqlpage.read_file_as_text('index.json')
        ELSE sqlpage.read_file_as_text('menu.json')
        END    AS properties;
*/
--Menu
SELECT 'dynamic' AS component,
  json_patch(
    CASE WHEN $group_id=1
          THEN sqlpage.read_file_as_text('index.json')
          ELSE sqlpage.read_file_as_text('menu.json')
     END,
    '{"style": "/style.css"}' -- Permet de personnaliser le tableau de synthèse
  ) AS properties;

-- Message si droits insuffisants sur une page
SELECT 'alert' as component,
    'Attention !' as title,
    'Vous ne possédez pas les droits suffisants pour accéder à cette page.' as description_md,
    'alert-circle' as icon,
    'red' as color
    WHERE $restriction IS NOT NULL;

--Boutons dont retour sans valider
SELECT
    'button' as component,
    'sm'     as size,
    'pill'   as shape;
    select
        'Tableau synthètique du suivi de l''élève' as title,
        'briefcase' as icon,
        'green' as color;
    select
        'Retour à la liste' as title,
        'eleves_suivi.sql' as link,
        'arrow-back-up' as icon,
        'green' as outline;


-- écrire les informations sur l'élève dans le haut de la page
SELECT
    'datagrid' as component,
        CASE WHEN EXISTS (SELECT eleve.id FROM image WHERE eleve.id=image.eleve_id)
            THEN image_url
            ELSE './icons/profil.png'
            END as image_url,
        UPPER(nom) || ' ' || prenom as title,
        'Sexe : '||sexe as description,
        'INE : '||INE as description_md
        FROM eleve LEFT JOIN image on image.eleve_id=eleve.id WHERE eleve.id = $id;
        select
            adresse||' - '||code_postal||' '||commune as title,
            'né(e) le : '||strftime('%d/%m/%Y',eleve.naissance)   as description, 'black' as color,
            0 as active
            FROM eleve LEFT JOIN image on image.eleve_id=eleve.id WHERE eleve.id = $id;
        select
            etab.type||' '||etab.nom_etab as title,
            'Classe : ' || classe as description,
            1 as active, 'green' as color,
            'etab_classes.sql?id='||etab.id||'&classe_select='||eleve.classe as link
            FROM eleve INNER JOIN etab on eleve.etab_id=etab.id WHERE eleve.id = $id;
        select
            'Niveau' as title,
            niveau as description,
            1 as active, 'orange' as color
            FROM eleve WHERE eleve.id = $id;

-- Affichage des besoins de l'élève
SET besoin1 = (SELECT besoin_prioritaire FROM besoins_eleve WHERE eleve_id=$id);
SET besoin2 = (SELECT besoin_secondaire FROM besoins_eleve WHERE eleve_id=$id);

SELECT 'alert' as component,
    'Aucun besoin n''est renseigné !' as title,
    'Cliquez sur le bouton pour le faire !' AS description_md,
    'info-square-rounded' as icon,
    TRUE as important,
    FALSE as dismissible,
    'red' as color,
    'eleve_besoins.sql?id='||$id as link,
    'Renseignez les besoins' as link_text
    WHERE $besoin1 is NULL;

SELECT
    'card' as component,
    2      as columns
    WHERE $besoin1 is NOT NULL;
    select
        'BESOIN PRIORITAIRE' as title,
        $besoin1 as description,
        'pentagon-number-1' as icon,
        'red' as color,
        TRUE as active
        WHERE $besoin1 is NOT NULL;
    select
        'BESOIN SECONDAIRE' as title,
        $besoin2 as description,
        'pentagon-number-2' as icon,
        'orange' as color
        WHERE $besoin1 is NOT NULL;

-- Bouton pour modifier les besoins
SELECT
    'button' as component,
    'sm'     as size,
    'pill'   as shape
    WHERE $besoin1 is NOT NULL;
    select
        'Modifier les besoins' as title,
        'eleve_besoins.sql?id='|| $id as link,
        'transform' as icon,
        'green' as outline,
        $group_id<2 as disabled
        WHERE $besoin1 is NOT NULL;


-- Titre au dessus de la liste des actions
SELECT 'title' as component,
   'Vue synthétique des actions mises en place' as contents,
   TRUE as center,
   2 as level;


-- Bouton pour ajouter une action de suivi_ajout
SELECT
    'button' as component,
    'sm'     as size,
    'pill'   as shape;
    select
        'Ajouter une action de suivi' as title,
        'suivi_ajout.sql?id='|| $id as link,
        'file-plus' as icon,
        'green' as outline,
        $group_id<2 as disabled;
    select
        'fiche linéaire des aides mises en place' as title,
        'suivi_eleve.sql?id='|| $id as link,
        'report' as icon,
        'green' as outline;

-- Légende
/*SELECT
    'text' as component,
   '##### Légende :
![](./icons/info-square-rounded.svg "PRISE D''INFORMATION") PRISE D''INFORMATION  / ![](./icons/keyframes.svg "DISPOSITIFS") DISPOSITIFS  / ![](./icons/circles-relation.svg "ALLIANCE") ALLIANCE
/ ![](./icons/affiliate.svg "PARTENAIRE EN") PARTENAIRES EN  / ![](./icons/adjustments-alt.svg "ORGANISATION DES AIDES") ORGANISATION DES AIDES / ![](./icons/directions.svg "ORIENTATION") ORIENTATION
' as contents_md;
*/
/*SELECT
    'text' as component,
   '##### Légende :' as contents_md;

SELECT
    'button' as component,
    'sm'     as size;
    --'square'   as shape;
    select
        'PRISE D''INFORMATION' as title,
        'Signaux qui justifient le suivi' as tooltip,
        'info-square-rounded' as icon,
        'yellow' as color,
        'yellow' as outline;
    select
        'DISPOSITIFS' as title,
        'Différenciation, groupe de besoin décloisonné, APC, RAN, Actions parents, Assoc partenaire' as tooltip,
        'keyframes' as icon,
        'blue' as color,
        'blue' as outline;
    select
        'ALLIANCE' as title,
        'Équipe éducative, autres' as tooltip,
        'circles-relation' as icon,
        'lime' as color,
        'lime' as outline;
    select
        'PARTENAIRES EN' as title,
        'RASED, Circo, Médecine scolaire, PMI' as tooltip,
        'affiliate' as icon,
        'teal' as color,
        'teal' as outline;
    select
        'ORGANISATION DES AIDES' as title,
        'PPRE, PAP, MDPH, demande bilan, suivi extérieur' as tooltip,
        'adjustments-alt' as icon,
        'gray-300' as color,
        'gray-300' as outline;
    select
        'ORIENTATION' as title,
        'Décision pour poursuite scolarité' as tooltip,
        'directions' as icon,
        'purple' as color,
        'purple' as outline;
*/

-- Tableaux
SET annee_en_cours=(SELECT annee FROM annee WHERE annee.active=1);
SET niveau_en_cours=(SELECT niveau FROM eleve WHERE eleve.id = $id);

SELECT 'table' as component,
       'Liste des actions de suivi menées tout au long de la scolarité' as title,
       --TRUE as striped_rows,
       TRUE as striped_columns,
       '1er question' as markdown, '1ère Scol' as markdown, 'FLE' as markdown, 'Assid.' as markdown, 'Mesure' as markdown,
       'Différenc.' as markdown, 'Gr.Besoin' as markdown, 'APC' as markdown, 'RAN' as markdown, 'Act.Parents' as markdown, 'Autr.Dispo' as markdown,
       'EE' as markdown, 'Autr.Alliance' as markdown,
       'RASED' as markdown, 'CIRCO' as markdown, 'Méd.Scol-PMI' as markdown,
       'PPRE' as markdown, 'PAP' as markdown, 'MDPH' as markdown, 'Dem.Bilan' as markdown, 'Suivi Ext.' as markdown,
       'Orient.' as markdown,
        1 as sort,
        1 as search,
        TRUE as hover,
        TRUE as small;
    select
        parcours.annee_id as 'Année',
        parcours.niveau as Niveau,
        format('[%s](%s "%s")',
            group_concat(CASE WHEN parcours.action_libelle='PRISE D''INFORMATION : 1er questionnement' THEN action_ordre END),
            group_concat(CASE WHEN parcours.action_libelle='PRISE D''INFORMATION : 1er questionnement' THEN 'suivi_edit.sql?id='||parcours.id||'&eleve_id='||$id END),
            group_concat(CASE WHEN parcours.action_libelle='PRISE D''INFORMATION : 1er questionnement' THEN action_commentaire END)
            ) as '1er question',
        format('[%s](%s "%s")',
            group_concat(CASE WHEN parcours.action_libelle='PRISE D''INFORMATION : 1ère scolarisation' THEN action_ordre END),
            group_concat(CASE WHEN parcours.action_libelle='PRISE D''INFORMATION : 1ère scolarisation' THEN 'suivi_edit.sql?id='||parcours.id||'&eleve_id='||$id END),
            group_concat(CASE WHEN parcours.action_libelle='PRISE D''INFORMATION : 1ère scolarisation' THEN action_commentaire END)
            ) as '1ère Scol',
        format('[%s](%s "%s")',
            group_concat(CASE WHEN parcours.action_libelle='PRISE D''INFORMATION : FLE' THEN action_ordre END),
            group_concat(CASE WHEN parcours.action_libelle='PRISE D''INFORMATION : FLE' THEN 'suivi_edit.sql?id='||parcours.id||'&eleve_id='||$id END),
            group_concat(CASE WHEN parcours.action_libelle='PRISE D''INFORMATION : FLE' THEN action_commentaire END)
            ) as 'FLE',
        format('[%s](%s "%s")',
            group_concat(CASE WHEN parcours.action_libelle='PRISE D''INFORMATION : Assiduité' THEN action_ordre END),
            group_concat(CASE WHEN parcours.action_libelle='PRISE D''INFORMATION : Assiduité' THEN 'suivi_edit.sql?id='||parcours.id||'&eleve_id='||$id END),
            group_concat(CASE WHEN parcours.action_libelle='PRISE D''INFORMATION : Assiduité' THEN action_commentaire END)
            ) as 'Assid.',
        format('[%s](%s "%s")',
            group_concat(CASE WHEN parcours.action_libelle='PRISE D''INFORMATION : Mesure' THEN action_ordre END),
            group_concat(CASE WHEN parcours.action_libelle='PRISE D''INFORMATION : Mesure' THEN 'suivi_edit.sql?id='||parcours.id||'&eleve_id='||$id END),
            group_concat(CASE WHEN parcours.action_libelle='PRISE D''INFORMATION : Mesure' THEN action_commentaire END)
            ) as 'Mesure',
        format('[%s](%s "%s")',
            group_concat (CASE WHEN parcours.action_libelle='DISPOSITIFS : Différenciation' THEN action_ordre END),
            group_concat(CASE WHEN parcours.action_libelle='DISPOSITIFS : Différenciation' THEN 'suivi_edit.sql?id='||parcours.id||'&eleve_id='||$id END),
            group_concat(CASE WHEN parcours.action_libelle='DISPOSITIFS : Différenciation' THEN action_commentaire END)
            ) as 'Différenc.',
        format('[%s](%s "%s")',
            group_concat (CASE WHEN parcours.action_libelle='DISPOSITIFS : Groupe de besoin décloisonné' THEN action_ordre END),
            group_concat(CASE WHEN parcours.action_libelle='DISPOSITIFS : Groupe de besoin décloisonné' THEN 'suivi_edit.sql?id='||parcours.id||'&eleve_id='||$id END),
            group_concat(CASE WHEN parcours.action_libelle='DISPOSITIFS : Groupe de besoin décloisonné' THEN action_commentaire END)
            ) as 'Gr.Besoin',
        format('[%s](%s "%s")',
            group_concat (CASE WHEN parcours.action_libelle='DISPOSITIFS : APC' THEN action_ordre END),
            group_concat(CASE WHEN parcours.action_libelle='DISPOSITIFS : APC' THEN 'suivi_edit.sql?id='||parcours.id||'&eleve_id='||$id END),
            group_concat(CASE WHEN parcours.action_libelle='DISPOSITIFS : APC' THEN action_commentaire END)
            ) as 'APC',
        format('[%s](%s "%s")',
            group_concat (CASE WHEN parcours.action_libelle='DISPOSITIFS : RAN' THEN action_ordre END),
            group_concat(CASE WHEN parcours.action_libelle='DISPOSITIFS : RAN' THEN 'suivi_edit.sql?id='||parcours.id||'&eleve_id='||$id END),
            group_concat(CASE WHEN parcours.action_libelle='DISPOSITIFS : RAN' THEN action_commentaire END)
            ) as 'RAN',
        format('[%s](%s "%s")',
            group_concat (CASE WHEN parcours.action_libelle='DISPOSITIFS : Action Parents' THEN action_ordre END),
            group_concat(CASE WHEN parcours.action_libelle='DISPOSITIFS : Action Parents' THEN 'suivi_edit.sql?id='||parcours.id||'&eleve_id='||$id END),
            group_concat(CASE WHEN parcours.action_libelle='DISPOSITIFS : Action Parents' THEN action_commentaire END)
            ) as 'Act.Parents',
        format('[%s](%s "%s")',
            group_concat (CASE WHEN parcours.action_libelle='DISPOSITIFS : Autre' THEN action_ordre END),
            group_concat(CASE WHEN parcours.action_libelle='DISPOSITIFS : Autre' THEN 'suivi_edit.sql?id='||parcours.id||'&eleve_id='||$id END),
            group_concat(CASE WHEN parcours.action_libelle='DISPOSITIFS : Autre' THEN action_commentaire END)
            ) as 'Autr.Dispo',
        format('[%s](%s "%s")',
            group_concat (CASE WHEN parcours.action_libelle='ALLIANCE : EE' THEN action_ordre END),
            group_concat(CASE WHEN parcours.action_libelle='ALLIANCE : EE' THEN 'suivi_edit.sql?id='||parcours.id||'&eleve_id='||$id END),
            group_concat(CASE WHEN parcours.action_libelle='ALLIANCE : EE' THEN action_commentaire END)
            ) as 'EE',
        format('[%s](%s "%s")',
            group_concat (CASE WHEN parcours.action_libelle='ALLIANCE : Autre alliance' THEN action_ordre END),
            group_concat(CASE WHEN parcours.action_libelle='ALLIANCE : Autre alliance' THEN 'suivi_edit.sql?id='||parcours.id||'&eleve_id='||$id END),
            group_concat(CASE WHEN parcours.action_libelle='ALLIANCE : Autre alliance' THEN action_commentaire END)
            ) as 'Autr.Alliance',
        format('[%s](%s "%s")',
            group_concat (CASE WHEN parcours.action_libelle='PARTENAIRES EN : RASED' THEN action_ordre END),
            group_concat(CASE WHEN parcours.action_libelle='PARTENAIRES EN : RASED' THEN 'suivi_edit.sql?id='||parcours.id||'&eleve_id='||$id END),
            group_concat(CASE WHEN parcours.action_libelle='PARTENAIRES EN : RASED' THEN action_commentaire END)
            ) as 'RASED',
        format('[%s](%s "%s")',
            group_concat (CASE WHEN parcours.action_libelle='PARTENAIRES EN : CIRCO' THEN action_ordre END),
            group_concat(CASE WHEN parcours.action_libelle='PARTENAIRES EN : CIRCO' THEN 'suivi_edit.sql?id='||parcours.id||'&eleve_id='||$id END),
            group_concat(CASE WHEN parcours.action_libelle='PARTENAIRES EN : CIRCO' THEN action_commentaire END)
            ) as 'CIRCO',
        format('[%s](%s "%s")',
            group_concat (CASE WHEN parcours.action_libelle='PARTENAIRES EN : Médecine scolaire - PMI' THEN action_ordre END),
            group_concat(CASE WHEN parcours.action_libelle='PARTENAIRES EN : Médecine scolaire - PMI' THEN 'suivi_edit.sql?id='||parcours.id||'&eleve_id='||$id END),
            group_concat(CASE WHEN parcours.action_libelle='PARTENAIRES EN : Médecine scolaire - PMI' THEN action_commentaire END)
            ) as 'Méd.Scol-PMI',
        format('[%s](%s "%s")',
            group_concat (CASE WHEN parcours.action_libelle='ORGANISATION DES AIDES : PPRE' THEN action_ordre END),
            group_concat(CASE WHEN parcours.action_libelle='ORGANISATION DES AIDES : PPRE' THEN 'suivi_edit.sql?id='||parcours.id||'&eleve_id='||$id END),
            group_concat(CASE WHEN parcours.action_libelle='ORGANISATION DES AIDES : PPRE' THEN action_commentaire END)
            ) as 'PPRE',
        format('[%s](%s "%s")',
            group_concat (CASE WHEN parcours.action_libelle='ORGANISATION DES AIDES : PAP' THEN action_ordre END),
            group_concat(CASE WHEN parcours.action_libelle='ORGANISATION DES AIDES : PAP' THEN 'suivi_edit.sql?id='||parcours.id||'&eleve_id='||$id END),
            group_concat(CASE WHEN parcours.action_libelle='ORGANISATION DES AIDES : PAP' THEN action_commentaire END)
            ) as 'PAP',
        format('[%s](%s "%s")',
            group_concat (CASE WHEN parcours.action_libelle='ORGANISATION DES AIDES : MDPH' THEN action_ordre END),
            group_concat(CASE WHEN parcours.action_libelle='ORGANISATION DES AIDES : MDPH' THEN 'suivi_edit.sql?id='||parcours.id||'&eleve_id='||$id END),
            group_concat(CASE WHEN parcours.action_libelle='ORGANISATION DES AIDES : MDPH' THEN action_commentaire END)
            ) as 'MDPH',
        format('[%s](%s "%s")',
            group_concat (CASE WHEN parcours.action_libelle='ORGANISATION DES AIDES : Demande Bilan' THEN action_ordre END),
            group_concat(CASE WHEN parcours.action_libelle='ORGANISATION DES AIDES : Demande Bilan' THEN 'suivi_edit.sql?id='||parcours.id||'&eleve_id='||$id END),
            group_concat(CASE WHEN parcours.action_libelle='ORGANISATION DES AIDES : Demande Bilan' THEN action_commentaire END)
            ) as 'Dem.Bilan',
        format('[%s](%s "%s")',
            group_concat (CASE WHEN parcours.action_libelle='ORGANISATION DES AIDES : Suivi extérieur' THEN action_ordre END),
            group_concat(CASE WHEN parcours.action_libelle='ORGANISATION DES AIDES : Suivi extérieur' THEN 'suivi_edit.sql?id='||parcours.id||'&eleve_id='||$id END),
            group_concat(CASE WHEN parcours.action_libelle='ORGANISATION DES AIDES : Suivi extérieur' THEN action_commentaire END)
            ) as 'Suivi Ext.',
        format('[%s](%s "%s")',
            group_concat (CASE WHEN parcours.action_libelle='ORIENTATION : Orientation' THEN action_ordre END),
            group_concat(CASE WHEN parcours.action_libelle='ORIENTATION : Orientation' THEN 'suivi_edit.sql?id='||parcours.id||'&eleve_id='||$id END),
            group_concat(CASE WHEN parcours.action_libelle='ORIENTATION : Orientation' THEN action_commentaire END)
            ) as 'Orient.'
        FROM parcours JOIN eleve on parcours.eleve_id=eleve.id Where parcours.eleve_id=$id AND ($group_id=4 or $group_id=1 or (eleve.classe=$classe_prof and $group_id=2) or (eleve.etab_id=$etab_prof and $group_id=3)) group by annee_id order by annee_id;




-- débuggage
--SELECT 'debug' as component, $id as eleve_id, $annee_en_cours as année, $niveau_en_cours as niveau;
