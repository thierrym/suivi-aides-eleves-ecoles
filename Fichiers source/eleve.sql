SELECT 'redirect' AS component,
        'signin.sql?error' AS link
 WHERE NOT EXISTS (SELECT 1 FROM login_session WHERE id=sqlpage.cookie('session'));
SET group_id = (SELECT user_info.groupe FROM login_session join user_info on user_info.username=login_session.username WHERE id = sqlpage.cookie('session'));
SET classe_prof = (SELECT user_info.classe FROM login_session join user_info on user_info.username=login_session.username WHERE id = sqlpage.cookie('session'));
SET etab_prof = (SELECT user_info.etab FROM login_session join user_info on user_info.username=login_session.username WHERE id = sqlpage.cookie('session'));

SELECT 'redirect' AS component,
        'parametres.sql?restriction' AS link
        WHERE $group_id<'3';

--Menu
SELECT 'dynamic' AS component, sqlpage.read_file_as_text('menu.json') AS properties;
    
    SELECT 
    'form' as component,
    'Créer un élève' as validate,
    'green'           as validate_color,
    'Recommencer'           as reset;
    
    SELECT 'SUIVI' AS label, 'suivi' AS name, 'checkbox' as type, 1 as checked, 1 as width;
    SELECT 'Nom' AS label, 'user' as prefix_icon, 'nom' AS name, 4 as width, TRUE as required;
    SELECT 'Prénom' AS label, 'user' as prefix_icon, 'prenom' AS name, 4 as width, TRUE as required;
    SELECT 'Date de naissance' AS label, 'calendar-month' as prefix_icon, 'naissance' AS name, 'date' as type, 3 as width;
    SELECT 'Sexe' AS label, 'friends' as prefix_icon, 'sexe' AS name, 'select' as type, '[{"label": "F", "value": "F"}, {"label": "M", "value": "M"}]' as options, 1 as width;
    SELECT 'Adresse' AS label, 'address-book' as prefix_icon, 'adresse' AS name, 'text' as type, 6 as width;
    SELECT 'Code Postal' AS label, 'mail' as prefix_icon, 'zipcode' AS name, 'text' as type, 2 as width;
    SELECT 'Commune' AS label, 'building-community' as prefix_icon, 'commune' AS name, 'text' as type, 4 as width;
    SELECT 'INE' AS label, 'barcode' as prefix_icon, 'ine' AS name, 'text' as type, 2 as width;
    SELECT 'Etablissement' AS name, 'select' as type, 3 as width, json_group_array(json_object("label", nom_etab, "value", id)) as options FROM (select nom_etab, id FROM etab union all
        select 'Aucun' as label, NULL as value
        ORDER BY nom_etab ASC);
    SELECT 'Niveau' AS name, 'select' as type, 2 as width, json_group_array(json_object("label", niv, "value", niv)) as options FROM (select niv, ordre FROM niveaux union all
        select '-' as label, NULL as value ORDER BY ordre ASC);
    SELECT 'Classe' AS label, 'users-group' as prefix_icon, 'classe' AS name, 2 as width;
    SELECT 'Enseignant' AS name, 'select' as type, 3 as width,
    json_group_array(json_object("label" , nom_ens, "value", id )) as options FROM (select nom_ens, id FROM enseignant union all
        select 'Aucun' as label, NULL as value ORDER BY nom_ens ASC);
    SELECT 'Commentaire' AS label,'textarea' as type, 'comm_eleve' AS name;
    
    -- Enregistrer l'élève créé dans la base
 INSERT INTO eleve(nom, prenom, naissance, sexe, adresse, code_postal, commune, INE, etab_id, niveau, classe, enseignant_id, comm_eleve, suivi) SELECT $nom, $prenom, $naissance, $sexe, $adresse ,$zipcode, $commune, $ine, :Etablissement, :Niveau, $classe, :Enseignant, $comm_eleve, $suivi WHERE $classe IS NOT NULL;

-- Liste des élèves
-- (we put it after the insertion because we want to see new accounts right away when they are created)
SELECT 'list' as component;
SELECT nom||' '||prenom as title, coalesce(type,'-')||' '||coalesce(nom_etab,'-')||' ('||coalesce(classe,'-')||')' as description,
    'notification.sql?id=' || eleve.id AS link
FROM eleve LEFT JOIN etab on eleve.etab_id=etab.id WHERE eleve.etab_id=$etab_prof ORDER by eleve.nom ASC;

