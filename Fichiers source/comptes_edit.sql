--Page permettant de modifier les informations sur les comptes des utilisateur⋅rices

SELECT 'redirect' AS component,
        'signin.sql?error' AS link
 WHERE NOT EXISTS (SELECT 1 FROM login_session WHERE id=sqlpage.cookie('session'));
SET group_id = (SELECT user_info.groupe FROM login_session join user_info on user_info.username=login_session.username WHERE id = sqlpage.cookie('session'));

SELECT 'redirect' AS component,
        'index.sql?restriction' AS link
        WHERE $group_id<>'4';

--Menu
SELECT 'dynamic' AS component, sqlpage.read_file_as_text('menu.json') AS properties;

--
-- Set variables 
SET nom_edit = (SELECT nom FROM user_info WHERE username = $id);
SET prenom_edit = (SELECT prenom FROM user_info WHERE username = $id);
SET groupe_edit = (SELECT groupe FROM user_info WHERE username = $id);
SET etab_edit = (SELECT etab FROM user_info WHERE username = $id);
SET classe_edit = (SELECT classe FROM user_info WHERE username = $id);
SET tel_edit = (SELECT tel FROM user_info WHERE username = $id);
SET courriel_edit = (SELECT courriel FROM user_info WHERE username = $id);
SET cas_edit = (SELECT CAS FROM user_info WHERE username = $id);

--Bouton retour sans valider
select 
    'button' as component,
    'sm'     as size,
    'pill'   as shape;
select 
    'Retour à la liste des comptes' as title,
    'comptes.sql' as link,
    'arrow-back-up' as icon,
    'green' as outline;  
/* 
-- En théorie l'administrateur ne devrait pas changer le mot de passe d'un utilisateur et ne devrait pas le connaître.
-- Cela reste techniquement possible en réactivant le bloc ci-dessous.
select 
    'Changer le mot de passe' as title,
    'comptes_edit_password.sql?id='||$id as link,
    'lock' as icon,
    'red' as outline;
*/  
select 
    'Nouveau code d''activation' as title,
    'comptes_edit_activation.sql?id='||$id as link,
    'lock' as icon,
    'red' as outline; 

-- Rappel du Compte concerné par la modification
SELECT 
    'alert' as component,
    'Alerte' as title,
    'Visualiser les changements opérés' as description,
    'alert-triangle' as icon,
    'green' as color;
    
SELECT 'table' as component;
    select 
        username as Identifiant,
        nom AS Nom,
        prenom AS Prénom,
        groupe AS Groupe,
        etab.nom_etab AS Établissement,
        $etab_edit_initial as ID,
        classe AS Classe,
        tel as Téléphone,
        courriel as courriel
        FROM user_info JOIN etab on user_info.etab=etab.id WHERE username=$id; 
    
--Formulaire de Mise à jour

--- Permet d'affecter une classe à un enseignant
SELECT 
    'form' as component,
    'Nouveau compte utilisateur' as title,
    --intitulé du bouton de validation
    CASE WHEN $groupe_edit is null
            THEN 'Continuer'
        WHEN CAST($groupe_edit as Integer)=2 and $classe_edit is NULL
            THEN 'Continuer'
        WHEN CAST($groupe_edit as Integer)=3 and $etab_edit is NULL
            THEN 'Continuer'
        WHEN CAST($etab_edit as Integer)<>$etab_edit_initial
            THEN 'continuer'
        WHEN CAST($groupe_edit as Integer)=1 or CAST($groupe_edit as Integer)=4
            THEN 'Mettre à jour ce compte'
        ELSE 'Mettre à jour ce compte'
        END as validate,
    -- action du bouton de validation
    CASE WHEN $groupe_edit is null
            THEN ''
        WHEN CAST($groupe_edit as Integer)=2 and $classe_edit is NULL
            THEN ''
        WHEN CAST($groupe_edit as Integer)=3 and $etab_edit is NULL
            THEN ''
        WHEN CAST($etab_edit as Integer)<>$etab_edit_initial
            THEN ''
        WHEN CAST($groupe_edit as Integer)=1 or CAST($groupe_edit as Integer)=4
            THEN 'comptes_edit_confirm.sql?id='||$id
        ELSE 'comptes_edit_confirm.sql?id='||$id
        END as action,
    -- couleur du bouton de validation
    CASE WHEN $groupe_edit is null
            THEN 'orange'
        WHEN CAST($groupe_edit as Integer)=2 and $classe_edit is NULL
            THEN 'orange'
        WHEN CAST($groupe_edit as Integer)=3 and $etab_edit is NULL
            THEN 'orange'
        WHEN CAST($groupe_edit as Integer)=1 or CAST($groupe_edit as Integer)=4
            THEN 'green'
        ELSE 'green'
        END as validate_color;


--- Remplissage des premier champs
    select 'Nom' AS label, 'nom' AS name, $nom_edit as value, 2 as width;
    SELECT 'Prénom' AS label, 'prenom' AS name, $prenom_edit as value, 2 as width;
    SELECT 'Téléphone' AS label, 'tel' AS name, $tel_edit as value, 2 as width;
    SELECT 'Courriel' AS label, 'courriel' AS name, $courriel_edit as value, 3 as width;
    -- SELECT 'Identifiant ENT' AS label, 'cas' AS name, $cas_edit as value, 3 as width;
    SELECT 'Droits :' AS label, 'groupe' AS name, CAST($groupe_edit as Integer) as value, 2 as width, 'select' as type, '[{"label": "Consultant RASED", "value": 1}, {"label": "Enseignant⋅e", "value": 2}, {"label": "Directeur⋅rice", "value": 3}, {"label": "Administrateur⋅rice", "value": 4}]' as options;
    
    SELECT 'Etablissement' AS label, 'etab' as name, 'select' as type, 2 as width, CAST($etab_edit as integer) as value, json_group_array(json_object("label", nom_etab, "value", id)) as options
        FROM (select nom_etab, id FROM etab union all select 'Aucun' as label, NULL as value ORDER BY nom_etab ASC)
            WHERE CAST($groupe_edit as Integer)=2 or CAST($groupe_edit as Integer)=3 having $groupe_edit is not null AND (CAST($groupe_edit as Integer)=2 or CAST($groupe_edit as Integer)=3);
   

    SELECT 'Classe' AS label, 'classe' as name, 'select' as type, 2 as width, $classe_edit as value, json_group_array(json_object('label', classe, 'value', classe)) as options FROM (select vue_classes.classe as classe, vue_classes.classe as value FROM vue_classes WHERE vue_classes.etab_id=$etab_edit UNION ALL SELECT 'Aucune' as label, NULL as value  ORDER BY vue_classes.classe ASC);
  


SELECT 'debug' as component, $groupe_edit as Groupe, CAST($etab_edit as Integer) as Etab, $classe_edit as Classe;


 
   
 

