-- création initiale des tables
CREATE TABLE cas_service(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    serveur TEXT,
    etat INTEGER
    );

CREATE TABLE eleve(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    nom TEXT,
    prenom TEXT,
    naissance DATE,
    sexe TEXT,
    INE TEXT UNIQUE,
    adresse TEXT,
    code_postal INTEGER,
    commune TEXT,
    etab_id INTEGER,
    etab_UAI TEXT,
    niveau TEXT,
    classe TEXT,
    dispositif TEXT,
    enseignant_id INTEGER,
    comm_eleve TEXT,
    modification TIMESTAMP,
    editeur TEXT,
    suivi INTEGER
    );

CREATE TABLE etab(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    type TEXT,
    UAI TEXT UNIQUE,
    nom_etab TEXT,
    Lon DECIMAL,
    Lat DECIMAL,
    description TEXT
    );

CREATE TABLE fiche (
	id	INTEGER PRIMARY KEY,
	titre	TEXT,
	contenu	TEXT,
	auteur	INTEGER,
	fiche_url	TEXT NOT NULL,
	created_at	TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	tag	TEXT
    );

CREATE TABLE image (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    eleve_id INTEGER,
    image_url TEXT NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
    );

CREATE TABLE login_session (
    id TEXT PRIMARY KEY,
    username TEXT NOT NULL REFERENCES user_info(username),
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    user_cas TEXT, token TEXT);

CREATE TABLE niveaux(
    niv TEXT PRIMARY KEY,
    ordre INTEGER
    );
INSERT INTO niveaux (niv, ordre)
VALUES ('TPS',1),('PS',2),('MS',3),('GS',4),('CP',5),('CE1',6),('CE2',7),('CM1',8),('CM2',9);

CREATE TABLE enseignant (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    username TEXT UNIQUE,
    nom_ens TEXT,
    prenom_ens TEXT,
    tel_ens TEXT,
    email TEXT,
    etab_ens INTEGER,
    classe_ens TEXT
);

CREATE TABLE user_info (
	username	TEXT PRIMARY KEY,
	password_hash	TEXT,
	nom	TEXT,
	prenom	TEXT,
	tel	TEXT,
	courriel	TEXT,
	groupe	INTEGER,
	connexion	TIMESTAMP DEFAULT Null,
	activation	TEXT DEFAULT Null,
	etab	INTEGER DEFAULT Null,
	classe TEXT,
	CAS TEXT
);

CREATE TABLE annee(
    annee TEXT PRIMARY KEY,
    active INTEGER
    );
INSERT INTO annee (annee,active)
VALUES ('2023-2024',1),('2024-2025',0),('2025-2026',0),('2026-2027',0),('2027-2028',0),('2028-2029',0),('2029-2030',0);


CREATE TABLE parcours(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    annee_id TEXT,
    eleve_id INTEGER,
    etab_id INTEGER,
    niveau TEXT,
    classe TEXT,
    enseignant_id INTEGER,
    action TEXT,
    action_categorie TEXT,
    action_libelle TEXT NOT NULL,
    action_ordre INTEGER,
    action_commentaire TEXT,
    horodatage DATE,
    dispositif_id INTEGER
    );

-- Vue des classes existantes avec l'établissement auquel elles sont rattachées
CREATE VIEW vue_classes AS
    SELECT DISTINCT
    classe,
    etab_id,
    etab_UAI
    FROM eleve;

-- Création de la table "besoins_eleve"
CREATE TABLE besoins_eleve(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    eleve_id INTEGER UNIQUE NOT NULL REFERENCES eleve(id),
    besoin_prioritaire TEXT,
    besoin_secondaire TEXT
    );

-- Création de la table "actions" et son peuplement recensant tous les types d'action de suivi
CREATE TABLE actions(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    action TEXT,
    categorie_action TEXT,
    libelle_action TEXT,
    info_action TEXT
    );
INSERT INTO actions (action, categorie_action, info_action)
VALUES ('1er questionnement','PRISE D''INFORMATION', 'Pour savoir à quel moment on s''est interrogé (booléen).'),
        ('1ère scolarisation','PRISE D''INFORMATION', 'Pour indiquer quand a eu lieu l''entrée à l''école (booléen).'),
        ('FLE','PRISE D''INFORMATION','Pour savoir si le français est langue étrangère (booléen).'),
        ('Assiduité','PRISE D''INFORMATION','Voir par rapport à l''absentéisme (booléen).'),
        ('Mesure','PRISE D''INFORMATION','élément(s) justifiant le suivi (booléen).'),
        ('Différenciation','DISPOSITIFS','Action pour différencier (integer).'),
        ('Groupe de besoin décloisonné','DISPOSITIFS','Mise en place de groupe de besoin (integer).'),
        ('APC','DISPOSITIFS','Mise en place d''APC (integer).'),
        ('RAN','DISPOSITIFS','Mise en place de RAN (integer).'),
        ('Action Parents','DISPOSITIFS','Mise en place de café, ateliers, APC, ... (integer).'),
        ('Autre','DISPOSITIFS','Association partenaire (integer).'),
        ('EE','ALLIANCE','Tenue d''équipe éducative (integer).'),
        ('Autre alliance','ALLIANCE','Autre alliance (integer).'),
        ('RASED','PARTENAIRES EN','Intervention du RASED (integer).'),
        ('CIRCO','PARTENAIRES EN','Intervention de l''équipe de circonscription (integer).'),
        ('Médecine scolaire - PMI','PARTENAIRES EN','Intervention de la médecine scolaire ou la PMI (integer).'),
        ('PPRE','ORGANISATION DES AIDES','Mise en place d''un PPRE (integer).'),
        ('PAP','ORGANISATION DES AIDES','Mise en place d''un PAP (integer).'),
        ('MDPH','ORGANISATION DES AIDES','Intervention de la MDPH (integer).'),
        ('Demande Bilan','ORGANISATION DES AIDES','Bilan à faire (integer).'),
        ('Suivi extérieur','ORGANISATION DES AIDES','Suivi extérieur (integer).'),
        ('Orientation','ORIENTATION','Orientation prévue par la suite (integer).')
;
UPDATE actions
set libelle_action=categorie_action ||' : '||action
;

;
