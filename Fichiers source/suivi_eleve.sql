-- Page permettant de lister toutes les actions du suivi d'un élève

-- Informations sur l'utilisateur⋅rice connecté
SET group_id = (SELECT user_info.groupe FROM login_session join user_info on user_info.username=login_session.username WHERE id = sqlpage.cookie('session'));
SET classe_prof = (SELECT user_info.classe FROM login_session join user_info on user_info.username=login_session.username WHERE id = sqlpage.cookie('session'));
SET etab_prof = (SELECT user_info.etab FROM login_session join user_info on user_info.username=login_session.username WHERE id = sqlpage.cookie('session'));

SELECT 'redirect' AS component,
        'index.sql?restriction' AS link
        WHERE $group_id<'1';

--Menu
SELECT 'dynamic' AS component,
    CASE WHEN $group_id=1
        THEN sqlpage.read_file_as_text('index.json')
        ELSE sqlpage.read_file_as_text('menu.json')
        END    AS properties;

-- Message si droits insuffisants sur une page
SELECT 'alert' as component,
    'Attention !' as title,
    'Vous ne possédez pas les droits suffisants pour accéder à cette page.' as description_md,
    'alert-circle' as icon,
    'red' as color
    WHERE $restriction IS NOT NULL;

--Boutons dont retour sans valider
SELECT
    'button' as component,
    'sm'     as size,
    'pill'   as shape;
    select
        'Suivi de l''élève' as title,
        'briefcase' as icon,
        'green' as color;
    select
        'Retour à la liste' as title,
        'eleves_suivi.sql' as link,
        'arrow-back-up' as icon,
        'green' as outline;


-- écrire les informations sur l'élève dans le haut de la page
SELECT
    'datagrid' as component,
        CASE WHEN EXISTS (SELECT eleve.id FROM image WHERE eleve.id=image.eleve_id)
            THEN image_url
            ELSE './icons/profil.png'
            END as image_url,
        UPPER(nom) || ' ' || prenom as title,
        'Sexe : '||sexe as description,
        'INE : '||INE as description_md
        FROM eleve LEFT JOIN image on image.eleve_id=eleve.id WHERE eleve.id = $id;
        select
            adresse||' - '||code_postal||' '||commune as title,
            'né(e) le : '||strftime('%d/%m/%Y',eleve.naissance)   as description, 'black' as color,
            0 as active
            FROM eleve LEFT JOIN image on image.eleve_id=eleve.id WHERE eleve.id = $id;
        select
            etab.type||' '||etab.nom_etab as title,
            'Classe : ' || classe as description,
            1 as active, 'green' as color,
            'etab_classes.sql?id='||etab.id||'&classe_select='||eleve.classe as link
            FROM eleve INNER JOIN etab on eleve.etab_id=etab.id WHERE eleve.id = $id;
        select
            'Niveau' as title,
            niveau as description,
            1 as active, 'orange' as color
            FROM eleve WHERE eleve.id = $id;

-- Affichage des besoins de l'élève
SET besoin1 = (SELECT besoin_prioritaire FROM besoins_eleve WHERE eleve_id=$id);
SET besoin2 = (SELECT besoin_secondaire FROM besoins_eleve WHERE eleve_id=$id);

SELECT 'alert' as component,
    'Aucun besoin n''est renseigné !' as title,
    'Cliquez sur le bouton pour le faire !' AS description_md,
    'info-square-rounded' as icon,
    TRUE as important,
    FALSE as dismissible,
    'red' as color,
    'eleve_besoins.sql?id='||$id as link,
    'Renseignez les besoins' as link_text
    WHERE $besoin1 is NULL;

SELECT
    'card' as component,
    2      as columns
    WHERE $besoin1 is NOT NULL;
    select
        'BESOIN PRIORITAIRE' as title,
        $besoin1 as description,
        'pentagon-number-1' as icon,
        'red' as color,
        TRUE as active
        WHERE $besoin1 is NOT NULL;
    select
        'BESOIN SECONDAIRE' as title,
        $besoin2 as description,
        'pentagon-number-2' as icon,
        'orange' as color
        WHERE $besoin1 is NOT NULL;

-- Bouton pour modifier les besoins
SELECT
    'button' as component,
    'sm'     as size,
    'pill'   as shape
    WHERE $besoin1 is NOT NULL;
    select
        'Modifier les besoins' as title,
        'eleve_besoins.sql?id='|| $id as link,
        'transform' as icon,
        'green' as outline,
        $group_id<2 as disabled
        WHERE $besoin1 is NOT NULL;


-- Titre au dessus de la liste des actions
SELECT 'title' as component,
   'Actions mises en place' as contents,
   TRUE as center,
   2 as level;


-- Bouton pour ajouter une action de suivi_ajout
SELECT
    'button' as component,
    'sm'     as size,
    'pill'   as shape;
    select
        'Ajouter une action de suivi' as title,
        'suivi_ajout.sql?id='|| $id as link,
        'file-plus' as icon,
        'green' as outline,
        $group_id<2 as disabled;
    select
        'Consulter fiche synthètique du suivi' as title,
        'suivi_eleve_synthese.sql?id='|| $id as link,
        'report' as icon,
        'green' as outline;


-- Légende
/*SELECT
    'text' as component,
   '##### Légende :
![](./icons/info-square-rounded.svg "PRISE D''INFORMATION") PRISE D''INFORMATION  / ![](./icons/keyframes.svg "DISPOSITIFS") DISPOSITIFS  / ![](./icons/circles-relation.svg "ALLIANCE") ALLIANCE
/ ![](./icons/affiliate.svg "PARTENAIRE EN") PARTENAIRES EN  / ![](./icons/adjustments-alt.svg "ORGANISATION DES AIDES") ORGANISATION DES AIDES / ![](./icons/directions.svg "ORIENTATION") ORIENTATION
' as contents_md;
*/
SELECT
    'text' as component,
   '##### Légende :' as contents_md;

SELECT
    'button' as component,
    'sm'     as size;
    --'square'   as shape;
    select
        'PRISE D''INFORMATION' as title,
        'Signaux qui justifient le suivi' as tooltip,
        'info-square-rounded' as icon,
        'yellow' as color,
        'yellow' as outline;
    select
        'DISPOSITIFS' as title,
        'Différenciation, groupe de besoin décloisonné, APC, RAN, Actions parents, Assoc partenaire' as tooltip,
        'keyframes' as icon,
        'blue' as color,
        'blue' as outline;
    select
        'ALLIANCE' as title,
        'Équipe éducative, autres' as tooltip,
        'circles-relation' as icon,
        'lime' as color,
        'lime' as outline;
    select
        'PARTENAIRES EN' as title,
        'RASED, Circo, Médecine scolaire, PMI' as tooltip,
        'affiliate' as icon,
        'teal' as color,
        'teal' as outline;
    select
        'ORGANISATION DES AIDES' as title,
        'PPRE, PAP, MDPH, demande bilan, suivi extérieur' as tooltip,
        'adjustments-alt' as icon,
        'gray-300' as color,
        'gray-300' as outline;
    select
        'ORIENTATION' as title,
        'Décision pour poursuite scolarité' as tooltip,
        'directions' as icon,
        'purple' as color,
        'purple' as outline;

SELECT 'table' as component,
       'Liste des actions de suivi menées' as title,
       TRUE as striped_rows,
       'Modif' as markdown,
       'icon' as icon,
        1 as sort,
        1 as search;
    select
        --parcours.id as ID,
        CASE
            WHEN (select INSTR(parcours.action_libelle,'PRISE D''INFORMATION') position) = 1 THEN 'yellow'
            WHEN (select INSTR(parcours.action_libelle,'DISPOSITIFS') position) = 1 THEN 'blue'
            WHEN (select INSTR(parcours.action_libelle,'ALLIANCE') position) = 1 THEN 'lime'
            WHEN (select INSTR(parcours.action_libelle,'PARTENAIRES EN') position) = 1 THEN 'teal'
            WHEN (select INSTR(parcours.action_libelle,'ORGANISATION DES AIDES') position) = 1 THEN 'gray-300'
            WHEN (select INSTR(parcours.action_libelle,'ORIENTATION') position) = 1 THEN 'purple'
            END as _sqlpage_color,
        parcours.annee_id as Année,
        parcours.niveau as Niveau,
        CASE
            WHEN (select INSTR(parcours.action_libelle,'PRISE D''INFORMATION') position) = 1 THEN 'info-square-rounded'
            WHEN (select INSTR(parcours.action_libelle,'DISPOSITIFS') position) = 1 THEN 'keyframes'
            WHEN (select INSTR(parcours.action_libelle,'ALLIANCE') position) = 1 THEN 'circles-relation'
            WHEN (select INSTR(parcours.action_libelle,'PARTENAIRES EN') position) = 1 THEN 'affiliate'
            WHEN (select INSTR(parcours.action_libelle,'ORGANISATION DES AIDES') position) = 1 THEN 'adjustments-alt'
            WHEN (select INSTR(parcours.action_libelle,'ORIENTATION') position) = 1 THEN 'directions'
            END as icon,
        parcours.action_libelle as 'Libellé des actions',
        parcours.action_commentaire as Commentaire,
        parcours.action_ordre as Priorité,
        strftime('%d/%m/%Y',parcours.horodatage) AS Date,
        CASE
            WHEN (eleve.suivi=1 and $group_id=2) THEN '[
                ![](./icons/edit.svg)](suivi_edit.sql?id='||parcours.id||'&eleve_id='||eleve.id||')
                [![](./icons/trash-off.svg)]()
                '
            WHEN (eleve.suivi=1 and $group_id>2) THEN '[
                ![](./icons/edit.svg)](suivi_edit.sql?id='||parcours.id||'&eleve_id='||eleve.id||')
                [![](./icons/trash.svg)](suivi_delete_confirm.sql?id='||parcours.id||'&eleve_id='||eleve.id||')
                '
            ELSE '[
                ![](./icons/pencil-off.svg)
                ]()[
                ![](./icons/trash-off.svg)
                ]()'
            END as Modif
        FROM parcours JOIN eleve on parcours.eleve_id=eleve.id Where parcours.eleve_id=$id AND ($group_id=4 or $group_id=1 or (eleve.classe=$classe_prof and $group_id=2) or (eleve.etab_id=$etab_prof and $group_id=3))
        order by parcours.annee_id, parcours.action_ordre;


/*
-- Liste des actions faites
SELECT
    'list' as component,
    'Liste des actions de suivi menées' as title,
    'Action non encore renseignées'  as empty_title,
    'Cette liste est vide. Cliquez ici pour créer une nouvelle action !' as empty_description,
    'suivi_ajout.sql?id='||$id as empty_link;

    select
        CASE
            WHEN action_categorie ='PRISE D''INFORMATION' THEN 'info-square-rounded'
            WHEN action_categorie='DISPOSITIFS' THEN 'blue'
            WHEN action_categorie='ALLIANCE' THEN 'lime'
            WHEN action_categorie='PARTENAIRES EN' THEN 'teal'
            WHEN action_categorie='ORGANISATION DES AIDES' THEN 'gray-300'
            WHEN action_categorie='ORIENTATION' THEN 'purple''grip-horizontal' as icon,
        CASE
            WHEN action_categorie ='PRISE D''INFORMATION' THEN 'yellow'
            WHEN action_categorie='DISPOSITIFS' THEN 'blue'
            WHEN action_categorie='ALLIANCE' THEN 'lime'
            WHEN action_categorie='PARTENAIRES EN' THEN 'teal'
            WHEN action_categorie='ORGANISATION DES AIDES' THEN 'gray-300'
            WHEN action_categorie='ORIENTATION' THEN 'purple'
            END as color,
        --strftime('%d/%m/%Y',horodatage)||' : '||action||'' as title,
        action as title,
        strftime('%d/%m/%Y',parcours.horodatage) ||' : '|| action_categorie as description_md,
        action_commentaire as description,
        'suivi_edit.sql?id='||$id as edit_link
        FROM parcours JOIN eleve on parcours.eleve_id=eleve.id WHERE parcours.eleve_id=$id order by parcours.horodatage ASC;
*/


-- débuggage
--SELECT 'debug' as component, $id as eleve_id, $besoin1 as besoin1, $besoin2 as besoin2, $modif as date, $edition as editeur, $confirmed as confirmed;
