-- Page permettant de modifier une aide existante pour l'élève

SELECT 'redirect' AS component,
        'signin.sql?error' AS link
        WHERE NOT EXISTS (SELECT 1 FROM login_session WHERE id=sqlpage.cookie('session'));

SET group_id = (SELECT user_info.groupe FROM login_session join user_info on user_info.username=login_session.username WHERE id = sqlpage.cookie('session'));

SELECT 'redirect' AS component,
        'index.sql?restriction' AS link
        WHERE $group_id<'2';

UPDATE eleve SET modification=$horodatage, editeur=$edition WHERE id=$eleve_id;

-- Mise à jour de l'action de suivi à l'élève
UPDATE parcours
    SET annee_id=$annee_id, eleve_id=$eleve_id, etab_id=$etab_id, niveau=$niveau, classe=$classe,
        enseignant_id=$enseignant_id, action=$action, action_categorie=$action_categorie,
        action_libelle=$action_libelle, action_ordre=$action_ordre, action_commentaire=$action_commentaire, horodatage=$horodatage
        WHERE parcours.id=$id
        RETURNING 'redirect' AS component,
            'suivi_eleve.sql?id='||$eleve_id as link;


SELECT 'debug' as component, $eleve_id as eleve_id, $id as parcours_id, $horodatage as date, $edition as editeur;
