-- Permet d'enregistrer les besoins d'un élève

SELECT 'redirect' AS component,
        'signin.sql?error' AS link
 WHERE NOT EXISTS (SELECT 1 FROM login_session WHERE id=sqlpage.cookie('session'));

SET group_id = (SELECT user_info.groupe FROM login_session join user_info on user_info.username=login_session.username WHERE id = sqlpage.cookie('session'));

SELECT 'redirect' AS component,
        'index.sql?restriction' AS link
        WHERE $group_id<'2';

-- ajoute les info de modification dans la table "eleve"
UPDATE eleve SET modification=$modif, editeur=$edition WHERE id=$eleve_id_besoin;

-- modifie les besoins de l'élève (à mettre tout au début à cause du redirect !!!)
UPDATE besoins_eleve SET besoin_prioritaire=$besoin1, besoin_secondaire=$besoin2 WHERE eleve_id=$eleve_id_besoin
    RETURNING
        'redirect' AS component,
        'suivi_eleve.sql?id='|| $eleve_id_besoin as link;


--Debug
--SELECT 'debug' as component, $eleve_id_besoin as eleve_id, $besoin1_edit as besoin1, $besoin2_edit as besoin2, $modif as date, $edition as editeur;
