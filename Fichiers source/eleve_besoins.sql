-- Page permettant de renseigner les besoins des élèves suivi⋅es

SELECT 'redirect' AS component,
        'signin.sql?error' AS link
 WHERE NOT EXISTS (SELECT 1 FROM login_session WHERE id=sqlpage.cookie('session'));

-- Informations sur l'utilisateur⋅rice connecté
SET group_id = (SELECT user_info.groupe FROM login_session join user_info on user_info.username=login_session.username WHERE id = sqlpage.cookie('session'));
SET classe_prof = (SELECT user_info.classe FROM login_session join user_info on user_info.username=login_session.username WHERE id = sqlpage.cookie('session'));
SET etab_prof = (SELECT user_info.etab FROM login_session join user_info on user_info.username=login_session.username WHERE id = sqlpage.cookie('session'));

SELECT 'redirect' AS component,
        'index.sql?restriction' AS link
        WHERE $group_id<'2';


--Menu
SELECT 'dynamic' AS component, sqlpage.read_file_as_text('index.json') AS properties;


--Bouton retour sans valider
select
    'button' as component,
    'sm'     as size,
    'pill'   as shape;
select
    'Retour à la liste' as title,
    'suivi_eleve.sql?id='||$id as link,
    'arrow-back-up' as icon,
    'green' as outline;

-- écrire le nom de l'élève dans le titre de la page
SELECT
    'datagrid' as component,
        CASE WHEN EXISTS (SELECT eleve.id FROM image WHERE eleve.id=image.eleve_id)
            THEN image_url
            ELSE './icons/profil.png'
            END as image_url,
        UPPER(nom) || ' ' || prenom as title,
        'Sexe : '||sexe as description,
        'INE : '||INE as description_md
        FROM eleve LEFT JOIN image on image.eleve_id=eleve.id WHERE eleve.id = $id;
        select
            adresse||' - '||code_postal||' '||commune as title,
            'né(e) le : '||strftime('%d/%m/%Y',eleve.naissance)   as description, 'black' as color,
            0 as active
            FROM eleve LEFT JOIN image on image.eleve_id=eleve.id WHERE eleve.id = $id;
        select
            etab.type||' '||etab.nom_etab as title,
            'Classe : ' || classe as description,
            1 as active, 'green' as color,
            'etab_classes.sql?id='||etab.id||'&classe_select='||eleve.classe as link
            FROM eleve INNER JOIN etab on eleve.etab_id=etab.id WHERE eleve.id = $id;
        select
            'Niveau' as title,
            niveau as description,
            1 as active, 'orange' as color
            FROM eleve WHERE eleve.id = $id;

-- Affichage des besoins de l'élève
SET edition = (SELECT user_info.username FROM login_session join user_info on user_info.username=login_session.username WHERE id = sqlpage.cookie('session') );
SET modif = (SELECT current_timestamp);
SET besoin1 = (SELECT besoin_prioritaire FROM besoins_eleve WHERE eleve_id=$id);
SET besoin2 = (SELECT besoin_secondaire FROM besoins_eleve WHERE eleve_id=$id);
SET eleve_id_besoin= $id;

-- Formulaire pour ajouter/modifier les besoins
SELECT 'form' as component,
    'Besoin(s) de l''élève' as title,
    CASE
        WHEN $besoin1 is NULL THEN
            'eleve_besoins_ajout.sql'
        ELSE
            'eleve_besoins_modif.sql'
        END as action,
    CASE
        WHEN $besoin1 is NULL THEN
            'Ajouter besoins'
        ELSE
            'Mettre à jour'
        END as validate,
    'green' as validate_color;
    select 'besoin1' as name, 'Besoin prioritaire' as label, 'textarea' AS type, 6 as width, $besoin1 as value, TRUE as required;
    select 'besoin2' as name, 'Besoin secondaire' as label, 'textarea' AS type, 6 as width, $besoin2 as value;
    select 'eleve_id_besoin' as name, 'Elève ID' as label, 'hidden' as type, 1 as width, $eleve_id_besoin as value, TRUE as readonly;
    select 'edition' as name, 'Édité par : ' as label, 'hidden' as type, 2 as width, $edition as value, TRUE as readonly;
    select 'modif' as name, 'date : ' as label, 'hidden' as type, 2 as width, $modif as value, TRUE as readonly;





-- débuggage
--SELECT 'debug' as component, $eleve_id_besoin as eleve_id, $besoin1 as besoin1, $besoin2 as besoin2, $modif as date, $edition as editeur;
