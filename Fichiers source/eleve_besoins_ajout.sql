-- Permet d'ajouter les besoins des élèves'

SELECT 'redirect' AS component,
        'signin.sql?error' AS link
        WHERE NOT EXISTS (SELECT 1 FROM login_session WHERE id=sqlpage.cookie('session'));

SET group_id = (SELECT user_info.groupe FROM login_session join user_info on user_info.username=login_session.username WHERE id = sqlpage.cookie('session'));

SELECT 'redirect' AS component,
        'index.sql?restriction' AS link
        WHERE $group_id<'2';

-- ajoute les info de modification dans la table "eleve"
UPDATE eleve SET modification=$modif, editeur=$edition WHERE id=$eleve_id_besoin;

-- ajouter des besoins à l'élève (au début à cause du redirect !!!)
INSERT INTO besoins_eleve(eleve_id, besoin_prioritaire, besoin_secondaire)
        SELECT $eleve_id_besoin, $besoin1, $besoin2
        RETURNING
            'redirect' AS component,
            'suivi_eleve.sql?id=' || $eleve_id_besoin AS link;


--Debuggage
--SELECT 'debug' as component, $eleve_id_besoin as eleve_id, $besoin1 as besoin1, $besoin2 as besoin2, $modif as date, $edition as editeur;
