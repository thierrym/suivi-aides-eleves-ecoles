SELECT 'redirect' AS component,
        'signin.sql?error' AS link
 WHERE NOT EXISTS (SELECT 1 FROM login_session WHERE id=sqlpage.cookie('session'));
SET group_id = (SELECT user_info.groupe FROM login_session join user_info on user_info.username=login_session.username WHERE id = sqlpage.cookie('session'));
SET classe_prof = (SELECT user_info.classe FROM login_session join user_info on user_info.username=login_session.username WHERE id = sqlpage.cookie('session'));
SET etab_prof = (SELECT user_info.etab FROM login_session join user_info on user_info.username=login_session.username WHERE id = sqlpage.cookie('session'));

SELECT 'redirect' AS component,
        'etablissement.sql?restriction' AS link
FROM eleve WHERE (SELECT user_info.etab FROM login_session join user_info on user_info.username=login_session.username WHERE id = sqlpage.cookie('session') and user_info.etab<>$id);

--Menu
SELECT 'dynamic' AS component, 
CASE WHEN $group_id=1
THEN sqlpage.read_file_as_text('index.json')
ELSE sqlpage.read_file_as_text('menu.json')
            END    AS properties; 


-- basculer vers notifications / Aesh
select 
    'button' as component,
    'sm'     as size,
    --'pill'   as shape,
    'center' as justify;

select 
    'Classes' as title,
    'etab_classes.sql?id=' || $id as link,
    'users-group' as icon,
    'orange' as outline;
select 
    'Carte' as title,
    'etab_carte.sql?id=' || $id as link,
    'map' as icon,
    'teal' as outline;
select 
    'Stats' as title,
    'etab_stats.sql?id=' || $id as link,
    'chart-histogram' as icon,
    'teal' as outline;
select 
    'Photos' as title,
    'etab_trombi.sql?id=' || $id as link,
    'camera' as icon,
    'teal' as outline;

-- Récupère les nombres d'élèves dans 2 variables 
SET NB_eleve = (SELECT count(distinct eleve.id) FROM eleve where eleve.etab_id=$id);
SET NB_suivi = (SELECT count(distinct eleve.id) FROM eleve where eleve.etab_id=$id AND eleve.suivi=1);

-- écrire les infos de l'établissement dans le titre de la page [GRILLE]
SELECT 
    'datagrid' as component,
    type || ' ' || nom_etab as title FROM etab WHERE id = $id;
    select 
        ' Élèves suivi⋅es : ' as title,
        $NB_suivi ||' sur ' || $NB_eleve as description,
        'briefcase' as icon;

--Onglets
SET tab=coalesce($tab,'Acc');
select 'tab' as component,
1 as center;
select  'Élèves avec Accompagnement'  as title, 'user-plus' as icon, 1  as active, 'etab_suivi.sql?id='||$id||'&tab=Acc' as link, CASE WHEN $tab='Acc' THEN 'orange' ELSE 'green' END as color;
select  'Élèves sans Accompagnement' as title, 'user-off' as icon, 0 as active, 'etab_suivi.sql?id='||$id||'&tab=SansAcc' as link, CASE WHEN $tab='SansAcc' THEN 'orange' ELSE 'green' END as color;
select  'Élèves en attente' as title, 'alert-triangle-filled' as icon, 1 as active, 'etab_suivi.sql?id='||$id||'&tab=Att' as link, CASE WHEN $tab='Att' THEN 'orange' ELSE 'green' END as color;
select  'Derniers changements' as title, 'clock' as icon, 1 as active, 'etab_suivi.sql?id='||$id||'&tab=Last' as link, CASE WHEN $tab='Last' THEN 'orange' ELSE 'green' END as color;

-- Liste des suivis avec accompagnement
select 
    'divider' as component,
    'Liste des élèves avec accompagnement' as contents,
    'orange' as color
        WHERE $tab='Acc';
    
SELECT 'table' as component,   
    'Actions' as markdown,
    1 as sort,
    1 as search
        WHERE $tab='Acc';
    SELECT 
    SUBSTR(eleve.prenom, 1, 1) ||'. '||eleve.nom as Élève,
    eleve.classe AS Classe,
    CASE WHEN $group_id>1 THEN
         '[
    ![](./icons/briefcase.svg)
](notification.sql?id='||eleve.id||'&tab=Profil)[
    ![](./icons/user-plus.svg)
](aesh_suivi.sql?id='||suivi.aesh_id||'&tab=Profils)' 
     ELSE '[
    ![](./icons/briefcase.svg)
](notification.sql?id='||eleve.id||'&tab=Profil)'
     END as Actions
FROM eleve INNER JOIN etab on eleve.etab_id = etab.id WHERE eleve.etab_id=$id and $tab='Acc' ORDER BY eleve.nom;
 
-- Télécharger les données
SELECT 
    'csv' as component,
    'Exporter' as title,
    'eleves_suivis_etablissement' as filename,
    'file-download' as icon,
    'green' as color
    WHERE $tab='Acc';
SELECT 
     eleve.nom as Nom,
      eleve.prenom as Prénom
FROM eleve INNER JOIN etab on eleve.etab_id = etab.id WHERE eleve.etab_id=$id and $tab='Acc' ORDER BY eleve.nom; 
  
  -- Liste des suivis sans accompagnement
select 
    'divider' as component,
    'Liste des élèves sans accompagnements' as contents,
    'orange' as color
        WHERE $tab='SansAcc';


-- Liste des derniers changements
select 
    'divider' as component,
    'Liste des derniers changements' as contents,
    'orange' as color
        WHERE $tab='Last';
    
SELECT 'table' as component,   
    'Actions' as markdown,
    1 as sort,
    1 as search
        WHERE $tab='Last';
SELECT 
    eleve.nom as Nom,
    eleve.prenom as Prénom,
    strftime('le %d/%m/%Y à %Hh%M:%S', eleve.modification) as Modifié,
    eleve.editeur AS Par
FROM eleve WHERE eleve.etab_id=$id and $tab='Last' ORDER BY eleve.modification DESC LIMIT 10;

