SELECT 'redirect' AS component,
        'signin.sql?error' AS link
 WHERE NOT EXISTS (SELECT 1 FROM login_session WHERE id=sqlpage.cookie('session'));
SET group_id = (SELECT user_info.groupe FROM login_session join user_info on user_info.username=login_session.username WHERE id = sqlpage.cookie('session'));
SELECT 'redirect' AS component,
        'eleve_edit.sql?id='||$id||'&restriction' AS link
        WHERE $group_id<'2';


-- Supprime l'ancienne photo
DELETE FROM image WHERE eleve_id=$id;

--
SET file_path = sqlpage.uploaded_file_path('Image');
--set file_name = sqlpage.random_string(10)||'.png';
SET file_name = './avatar/'||sqlpage.random_string(10)||'.jpg';
SET mv_result = sqlpage.exec('mv', $file_path, $file_name);


-- ajouter une photo
INSERT OR ignore INTO image (eleve_id, image_url)
    values (
        $id,
        $file_name
        )
    returning
    'redirect' AS component,
    'eleve_edit.sql?id='||$id as link;

-- If the insert failed, warn the user
SELECT 'alert' as component,
    'red' as color,
    'alert-triangle' as icon,
    'Failed to upload image' as title,
    'Please try again with a smaller picture. Maximum allowed file size is 500Kb.' as description;
