SELECT 'redirect' AS component,
        'signin.sql?error' AS link
 WHERE NOT EXISTS (SELECT 1 FROM login_session WHERE id=sqlpage.cookie('session'));

SET group_id = (SELECT user_info.groupe FROM login_session join user_info on user_info.username=login_session.username WHERE id = sqlpage.cookie('session'));

SELECT 'redirect' AS component,
        'index.sql?restriction' AS link
        WHERE $group_id<>'4';

--Menu
SELECT 'dynamic' AS component, sqlpage.read_file_as_text('menu.json') AS properties;

select
    'form'       as component,
    'Importer des établissement' as title,
    'Envoyer'  as validate,
    './etab_csv_upload.sql' as action;
select
    'etab_data_input' as name,
    'file'               as type,
    'text/csv'           as accept,
    'Fichier .csv pour importer des établissements'           as label,
    'Envoyer un fichier CSV avec ces colonnes séparées par des points virgules et encodé en UTF-8 : id, type, UAI, nom_etab, Lon, Lat, description' as description,
    TRUE                 as required;

-- Télécharger les données
-- Variable pour compter le nombre d'établissements dans la base
SET NB_etab = (SELECT count(etab.id) FROM etab);
select
    'divider' as component,
    'Outils d''importations'   as contents;
SELECT
    'csv' as component,
    'Exporter le fichier des établissements ' as title,
    'etablissements' as filename,
    'file-download' as icon,
    'green' as color,
    ';' as separator
    WHERE $NB_etab > 0;
SELECT
    id as id,
    type as type,
    UAI as UAI,
    nom_etab as nom_etab,
    Lon as Lon,
    Lat as Lat,
    description as description
    FROM etab ORDER BY etab.nom_etab ASC;
