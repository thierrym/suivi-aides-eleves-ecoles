SELECT 'redirect' AS component,
        'signin.sql?error' AS link
 WHERE NOT EXISTS (SELECT 1 FROM login_session WHERE id=sqlpage.cookie('session'));
SET group_id = (SELECT user_info.groupe FROM login_session join user_info on user_info.username=login_session.username WHERE id = sqlpage.cookie('session'));

SELECT 'redirect' AS component,
        'notification.sql?id='||$id||'&restriction' AS link
        WHERE $group_id<'2';

--Menu
SELECT 'dynamic' AS component, sqlpage.read_file_as_text('menu.json') AS properties;

--Bouton retour sans valider
select
    'button' as component,
    'sm'     as size,
    'pill'   as shape;
select
    'Retour à la liste' as title,
    'eleves_suivi.sql' as link,
    'arrow-back-up' as icon,
    'green' as outline;
select
    'Retour à la fiche élève' as title,
    'suivi_eleve.sql?id='|| $id || '' as link,
    'briefcase' as icon,
    'green' as outline;

-- écrire le nom de l'élève dans le titre de la page
SELECT
    'datagrid' as component,
    CASE WHEN EXISTS (SELECT eleve.id FROM image WHERE eleve.id=image.eleve_id)
        THEN image_url
        ELSE './icons/profil.png'
        END as image_url,
    UPPER(nom) || ' ' || prenom as title,
    'Sexe : '||sexe as description,
    'INE : '||INE as description_md
    FROM eleve LEFT JOIN image on image.eleve_id=eleve.id WHERE eleve.id = $id;

    select
        adresse||' - '||code_postal||' '||commune as title,
        'né(e) le : '||strftime('%d/%m/%Y',eleve.naissance)   as description, 'black' as color,
        0 as active
        FROM eleve LEFT JOIN image on image.eleve_id=eleve.id WHERE eleve.id = $id;
    select
        etab.type||' '||etab.nom_etab as title,
        'Classe : ' || classe as description,
        1 as active, 'green' as color,
        'etab_classes.sql?id='||etab.id||'&classe_select='||eleve.classe as link
        FROM eleve INNER JOIN etab on eleve.etab_id=etab.id WHERE eleve.id = $id;
    select
        'Niveau' as title,
        niveau as description,
        1 as active, 'orange' as color
        FROM eleve WHERE eleve.id = $id;


-- Récupère les informations utiles de l'élève
SET annee_id = (SELECT annee FROM annee WHERE annee.active = 1);
SET eleve_id = (SELECT id FROM eleve WHERE id = $id);
SET etab_id = (SELECT etab_id FROM eleve WHERE eleve.id = $id);
SET niveau = (SELECT niveau FROM eleve WHERE eleve.id = $id);
SET classe = (SELECT classe FROM eleve WHERE eleve.id = $id);
SET enseignant_id = (SELECT enseignant_id FROM eleve WHERE eleve.id = $id);
-- Récupère les informations sur la création de ce suivi
SET edition = (SELECT user_info.username FROM login_session join user_info on user_info.username=login_session.username WHERE id = sqlpage.cookie('session') )
SET horodatage = (SELECT current_timestamp)


-- Formulaire pour ajouter un suivi
SELECT 'form' as component,
    'Mise en place d''une action pour le suivi' as title,
    'suivi_ajout_confirm.sql?id='||$id||'' as action,
    'Ajouter cette action' as validate,
    'green'           as validate_color;
    --'Recommencer'           as reset;
    select 'eleve_id' as name, 'Élève ID' as label, 'hidden' as type, 1 as width, $eleve_id as value, TRUE as readonly;
    select 'annee_id' as name, 'Année scolaire' as label, 'select' AS type, 2 as width, $annee_id as value,
        json_group_array(json_object("label", annee, "value", annee)) as options FROM (select annee FROM annee ORDER BY annee DESC);
    select 'niveau' AS name, 'Niveau' AS label, 'select' as type, 1 as width, $niveau as value,
        json_group_array(json_object("label", niv, "value", niv)) as options FROM (select niv FROM niveaux ORDER BY niveaux.ordre);
    select 'action_libelle' As name, 'Action' As label, 'select' as type, 3 as width, TRUE as required,
        json_group_array(json_object("label", action, "value", valeur, "ordre", num_ordre)) as options FROM (select libelle_action as action, libelle_action as valeur, actions.id as num_ordre FROM actions UNION ALL SELECT 'Aucune' as label, NULL as value, 0 as num_ordre order by num_ordre);
    select 'action_ordre' As name, 'Ordre de cette action (de 1 à 3)' As label, 'clock' as prefix_icon, 'number' as type, 1 as step, 1 as min, 3 as max, 2 as width, TRUE as required;
    select 'action_commentaire' As name, 'Commentaire' As label, 'textarea' as type, 12 as width;
    select 'edition' as name, 'Édité par : ' as label, 'hidden' as type, 2 as width, $edition as value, TRUE as readonly;
    select 'horodatage' as name, 'date : ' as label, 'hidden' as type, 2 as width, $horodatage as value, TRUE as readonly;



-- débuggage
SELECT 'debug' as component, $eleve_id as eleve_id;
