-- Page pour lister et gérer les enseignant⋅es

SELECT 'redirect' AS component,
        'signin.sql?error' AS link
 WHERE NOT EXISTS (SELECT 1 FROM login_session WHERE id=sqlpage.cookie('session'));
SET group_id = (SELECT user_info.groupe FROM login_session join user_info on user_info.username=login_session.username WHERE id = sqlpage.cookie('session'));

SELECT 'redirect' AS component,
        'etablissement.sql?restriction' AS link
        WHERE $group_id<'2';

--Menu
SELECT 'dynamic' AS component, sqlpage.read_file_as_text('menu.json') AS properties;

-- Message si droits insuffisants sur une page
SELECT 'alert' as component,
    'Attention !' as title,
    'Vous ne possédez pas les droits suffisants pour accéder à cette page.' as description_md,
    'alert-circle' as icon,
    'red' as color
WHERE $restriction IS NOT NULL;

--Sous-menu
select
    'button' as component,
    'sm'     as size,
    'pill'   as shape,
    'center' as justify;
select
    'Enseignant⋅e' as title,
    'writing' as icon,
    'orange' as color;
select
    'Établissements' as title,
    'etablissement.sql' as link,
    'building-community' as icon,
    'orange' as outline;
select
    'Niveaux' as title,
    'niveaux.sql' as link,
    'stairs' as icon,
    'Orange' as color,
    'orange' as outline;


-- Récupère l'établissement auquel est rattaché⋅e la personne réalisant l'ajout (directeur⋅rice ou admin)
SET etab_user=(SELECT user_info.etab FROM login_session join user_info on user_info.username=login_session.username WHERE id = sqlpage.cookie('session'));
SET etab_ens=$etab_user;


-- Sous Menu Enseignant⋅e
SELECT
    'button' as component,
    'sm'     as size,
    'pill'   as shape;
    select
        'Ajouter un⋅e enseignant⋅e' as title,
        'enseignant_ajout.sql' as link,
        'square-rounded-plus' as icon,
        $group_id<3 as disabled,
        'green' as outline;

---------------------------
SELECT 'table' as component,
    'Actions' as markdown,
    1 as sort,
    1 as search;

    select
        nom_ens AS Nom,
        prenom_ens AS Prénom,
        CASE WHEN $group_id>2
            THEN    tel_ens
            ELSE 'numéro masqué'
            END as Téléphone,
        email as courriel,
        etab.nom_etab as Établissement,
        classe_ens As Classe,
        CASE WHEN $group_id>2 THEN
            activation
            ELSE 'non visible'
            END AS 'Code Activation',
        CASE WHEN $group_id>2 THEN
            '[
            ![](./icons/pencil.svg)
            ](enseignant_edit.sql?id='||enseignant.id||'&username='||enseignant.username||') [
            ![](./icons/trash.svg)
            ](enseignant_delete.sql?id='||enseignant.id||')'
            ELSE
            '[
            ![](./icons/pencil-off.svg)]() [
            ![](./icons/trash-off.svg)]()'
            END as Actions
    FROM enseignant LEFT JOIN etab on enseignant.etab_ens=etab.id LEFT JOIN user_info on enseignant.username=user_info.username WHERE enseignant.etab_ens=$etab_user or $group_id=4 GROUP BY enseignant.id ORDER BY enseignant.nom_ens ASC;
