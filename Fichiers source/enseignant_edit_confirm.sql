SELECT 'redirect' AS component,
        'signin.sql?error' AS link
 WHERE NOT EXISTS (SELECT 1 FROM login_session WHERE id=sqlpage.cookie('session'));
SET group_id = (SELECT user_info.groupe FROM login_session join user_info on user_info.username=login_session.username WHERE id = sqlpage.cookie('session'));
SELECT 'redirect' AS component,
        'enseignant.sql?restriction' AS link
        WHERE $group_id<'3';

   -- Mettre à jour l'enseignant⋅e modifié dans la base
 UPDATE enseignant SET nom_ens=$nom, prenom_ens=$prenom, tel_ens=$tel, email=$email, etab_ens=$etab_ens, classe_ens=$classe_ens WHERE id=$id and $nom is not null;
     -- Mettre à jour le compte modifié dans la base
 UPDATE user_info SET nom=$nom, prenom=$prenom, tel=$tel, courriel=$email, etab=$etab_ens, classe=$classe_ens WHERE username=$username and $nom is not null
 RETURNING
 'redirect' as component,
 'enseignant.sql' as link;


