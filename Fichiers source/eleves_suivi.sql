-- Page listant les élèves faisant l'objet d'un suivi

SELECT 'redirect' AS component,
        'signin.sql?error' AS link
 WHERE NOT EXISTS (SELECT 1 FROM login_session WHERE id=sqlpage.cookie('session'));
SET group_id = (SELECT user_info.groupe FROM login_session join user_info on user_info.username=login_session.username WHERE id = sqlpage.cookie('session'));
SET classe_prof = (SELECT user_info.classe FROM login_session join user_info on user_info.username=login_session.username WHERE id = sqlpage.cookie('session'));
SET etab_prof = (SELECT user_info.etab FROM login_session join user_info on user_info.username=login_session.username WHERE id = sqlpage.cookie('session'));

--Menu
SELECT 'dynamic' AS component,
    CASE WHEN $group_id=1
        THEN sqlpage.read_file_as_text('index.json')
        ELSE sqlpage.read_file_as_text('menu.json')
        END    AS properties;

-- Message si droits insuffisants sur une page
SELECT 'alert' as component,
    'Attention !' as title,
    'Vous ne possédez pas les droits suffisants pour accéder à cette page.' as description_md,
    'alert-circle' as icon,
    'red' as color
WHERE $restriction IS NOT NULL;

-- Titre de la page
SELECT 'title' as component,
   'Liste des élèves suivi⋅es' as contents,
   TRUE as center,
   2 as level;

-- Bouton en haut de page
SELECT
    'button' as component,
    'sm'     as size,
    'pill'   as shape;
    select
        'Élèves SUIVI⋅ES' as title,
        'briefcase' as icon,
        'green' as color;

-- Légende
SELECT
    'text' as component,
   '##### Légende :
![](./icons/list.svg) Aides mises en place / ![](./icons/report.svg) Synthèse du suivi
' as contents_md;

-- Liste des élèves
SELECT 'table' as component,
    'Suivis' AS markdown,
    'admin' AS markdown,
    1 as sort,
    1 as search;

    select
        eleve.nom as Nom,
        eleve.prenom as Prénom,
        strftime('%d/%m/%Y',eleve.naissance) AS Naissance,
        eleve.niveau as Niveau,
        eleve.classe as Classe,
        etab.nom_etab as Établissement,
        CASE
            WHEN EXISTS (SELECT id FROM eleve WHERE eleve.suivi=1)
            THEN  '[
                 ![](./icons/list.svg "Voir le suivi")](suivi_eleve.sql?id='||eleve.id||')
                 [
                 ![](./icons/report.svg "Synthèse du suivi")](suivi_eleve_synthese.sql?id='||eleve.id||')
                '
            END as Suivis,
        CASE
            WHEN ($group_id>1 and $group_id<4) THEN '[
                ![](./icons/trash-off.svg)]()
                [
                ![](./icons/pencil.svg)](eleve_edit.sql?id='||eleve.id||')
                '
            WHEN $group_id=4 THEN '[
                ![](./icons/trash.svg)](eleve_delete.sql?id='||eleve.id||')
                [
                ![](./icons/pencil.svg)](eleve_edit.sql?id='||eleve.id||')
                '
            ELSE '[
                ![](./icons/trash-off.svg)]()
                [
                ![](./icons/pencil-off.svg)]()
                '
            END as admin
    FROM eleve INNER JOIN etab on eleve.etab_id=etab.id WHERE eleve.suivi=1 AND ($group_id=4 OR $group_id=1 or (eleve.classe=$classe_prof and $group_id=2) or (eleve.etab_id=$etab_prof and $group_id=3)) GROUP BY eleve.id ORDER BY eleve.nom ASC;
