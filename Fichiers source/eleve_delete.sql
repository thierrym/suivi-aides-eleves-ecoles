SELECT 'redirect' AS component,
        'signin.sql?error' AS link
 WHERE NOT EXISTS (SELECT 1 FROM login_session WHERE id=sqlpage.cookie('session'));
SET group_id = (SELECT user_info.groupe FROM login_session join user_info on user_info.username=login_session.username WHERE id = sqlpage.cookie('session'));
SELECT 'redirect' AS component,
        'notification.sql?restriction&id='||$id AS link
        WHERE $group_id<'3';

--Menu
SELECT 'dynamic' AS component, sqlpage.read_file_as_text('menu.json') AS properties;

--Bouton retour sans valider
select 
    'button' as component,
    'sm'     as size,
    'pill'   as shape;
select 
    'Retour à la liste' as title,
    'eleves.sql' as link,
    'arrow-back-up' as icon,
    'green' as outline;  
select 
    'Retour à la fiche élève' as title,
    'notification.sql?id='|| $id||'&tab=Profil' as link,
    'briefcase' as icon,
    'green' as outline;     

SELECT 
    'alert' as component,
    'Alerte' as title,
    'Toute suppression est définitive !' as description,
    'alert-triangle' as icon,
    'red' as color;
-- Set a variable 
--SET var_suivi = (SELECT count(suivi.id) FROM suivi where suivi.eleve_id=$id); 
--SET var_amenag = (SELECT count(amenag.id) FROM amenag where amenag.eleve_id=$id); 
--SET var_notif = (SELECT count(notification.id) FROM notification where notification.eleve_id=$id); 
      
-- Isolement de l'élève dans une liste
SELECT 'table' as component,
    'actions' AS markdown,
    1 as sort,
    1 as search;
    
SELECT 
      eleve.nom as Nom,
      eleve.prenom as Prénom,
      strftime('%d/%m/%Y',eleve.naissance) AS Naissance,
  eleve.classe as Classe,
  etab.nom_etab as Établissement,
--  CASE WHEN $var_suivi>=1 OR $var_amenag>=1 OR $var_notif>=1 
--THEN
--'[
--    ![](./icons/trash-off.svg)
--]() ' 
--ELSE
      '[
    ![](./icons/trash.svg)
](eleve_delete_confirm.sql?id='||eleve.id||')'
--END 
as actions
FROM eleve LEFT JOIN etab on eleve.etab_id=etab.id Where eleve.id=$id;
