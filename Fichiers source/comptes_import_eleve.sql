SELECT 'redirect' AS component,
        'signin.sql?error' AS link
 WHERE NOT EXISTS (SELECT 1 FROM login_session WHERE id=sqlpage.cookie('session'));

SET group_id = (SELECT user_info.groupe FROM login_session join user_info on user_info.username=login_session.username WHERE id = sqlpage.cookie('session'));

SELECT 'redirect' AS component,
        'index.sql?restriction' AS link
        WHERE $group_id<>'4';

--Menu
SELECT 'dynamic' AS component, sqlpage.read_file_as_text('menu.json') AS properties;


--Avertissement au cas où il n'y aurait pas d'établissement déjà renseigné dans la base
SET NB_etab = (SELECT count(etab.id) FROM etab);
SELECT 'text' AS component
        WHERE $NB_etab=0;
select  'ATTENTION : il n''y a aucun établissement de renseigné dans la base !!!' AS contents,
        'red' AS color, TRUE AS bold WHERE $NB_etab=0;
select '**[Compléter la liste des établissements](etablissement.sql "Lien vers la page des établissements")**' AS contents_md WHERE $NB_etab=0;


-- Ajout d'un bouton pour les établissements'
select
    'button' as component,
    'sm'     as size,
    'center' as justify,
    'pill'   as shape;
select
    'Liste des établissements' as title,
    'building-community' as icon,
    'green' as color,
    'etablissement.sql' as link;

-- Formulaire d'importation
select
    'form'       as component,
    'Importer des élèves' as title,
    'Envoyer'  as validate,
    './comptes_upload_eleve_TM.sql' as action;
select
    'comptes_data_input' as name,
    'file'               as type,
    'text/csv'           as accept,
    'Fichier .csv de ONDE pour importer les élèves'           as label,
    'Envoyer un fichier CSV avec ces colonnes séparées par des points virgules et encodé en UTF-8 : nom, prenom, naissance, sexe, INE, adresse, code, commune, etab_id, classe, niveau' as description,
    TRUE                 as required;
select
    'etablissement_id' AS name, 'Établissement d''où provient l''extraction ONDE' as label, 'select' as type, 3 as width, json_group_array(json_object("label", nom_etab, "value", id)) as options FROM (select nom_etab, id FROM etab union all
   select 'Aucun' as label, NULL as value
 ORDER BY nom_etab ASC);


-- Télécharger les données
select
    'divider' as component,
    'Outils d''importations'   as contents;
SELECT
    'csv' as component,
    'Exporter le fichier des élèves ' as title,
    'eleves' as filename,
    'file-download' as icon,
    ';' as separator,
    'green' as color;
SELECT
    nom as nom,
    prenom as prenom,
    naissance as naissance,
    sexe as sexe,
    INE as INE,
    adresse as adresse,
    code_postal as code,
    commune as commune,
    etab_id as etab_id,
    classe as classe,
    niveau as niveau
  FROM eleve ORDER BY eleve.nom ASC;

SELECT
    'csv' as component,
    'Nomenclature des Niveaux de classe ' as title,
    'niveaux' as filename,
    'file-download' as icon,
    ';' as separator,
    'green' as color;
SELECT
    niv as niveaux,
    ordre as ordre
  FROM niveaux ORDER BY ordre ASC;


SELECT
    'csv' as component,
    'Exporter le fichier des établissements ' as title,
    'établissements' as filename,
    'file-download' as icon,
    'green' as color,
    ';' as separator
    WHERE $NB_etab>0;
SELECT
    id as id,
    type as type,
    UAI as UAI,
    nom_etab as nom_etab,
    Lon as Lon,
    Lat as Lat,
    description as description
  FROM etab WHERE $NB_etab>0 ORDER BY etab.nom_etab ASC;
