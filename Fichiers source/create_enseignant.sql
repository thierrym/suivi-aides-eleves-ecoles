-- Page pour créer à la fois un⋅e esneignant⋅e et un⋅e utilisateur⋅rice

SELECT 'redirect' AS component,
        'signin.sql?error' AS link
 WHERE NOT EXISTS (SELECT 1 FROM login_session WHERE id=sqlpage.cookie('session'));

SET group_id = (SELECT user_info.groupe FROM login_session join user_info on user_info.username=login_session.username WHERE id = sqlpage.cookie('session'));

SELECT 'redirect' AS component,
        'index.sql?restriction' AS link
        WHERE $group_id<'3';

-- Enregistrer l'enseignant' créé dans la base
 INSERT INTO enseignant(username, nom_ens, prenom_ens, tel_ens, email, etab_ens, classe_ens)
    VALUES (:username, :nom, :prenom, :tel_ens, :email, :etab_ens, :classe_ens)
    ON CONFLICT (username) DO NOTHING;
      -- Enregistrer l'enseignant créé dans les comptes utilisateurs
INSERT INTO user_info (username, activation, nom, prenom, groupe, tel, courriel, etab, classe)
    VALUES (:username, :activation, :nom, :prenom, :groupe, :tel_ens, :email, :etab_ens, :classe_ens)
    ON CONFLICT (username) DO NOTHING
    RETURNING
     'redirect' AS component,
    'create_enseignant_welcome_message.sql?username=' || :username AS link;

-- If we are still here, it means that the user was not created
-- because the username was already taken.
SELECT 'redirect' AS component, 'create_enseignant_welcome_message.sql?error&username=' || :username AS link;
