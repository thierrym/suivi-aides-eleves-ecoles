-- Page permettant d'éditer les informations administratives d'un élève

SELECT 'redirect' AS component,
        'signin.sql?error' AS link
 WHERE NOT EXISTS (SELECT 1 FROM login_session WHERE id=sqlpage.cookie('session'));

-- Informations sur l'utilisateur⋅rice connecté
SET group_id = (SELECT user_info.groupe FROM login_session join user_info on user_info.username=login_session.username WHERE id = sqlpage.cookie('session'));
SET classe_prof = (SELECT user_info.classe FROM login_session join user_info on user_info.username=login_session.username WHERE id = sqlpage.cookie('session'));
SET etab_prof = (SELECT user_info.etab FROM login_session join user_info on user_info.username=login_session.username WHERE id = sqlpage.cookie('session'));


SELECT 'redirect' AS component,
        'index.sql?restriction' AS link
        WHERE $group_id<'2';

   -- Mettre à jour l'élève modifié dans la base
SET edition = (SELECT user_info.username FROM login_session join user_info on user_info.username=login_session.username WHERE id = sqlpage.cookie('session') )
SET modif = (SELECT current_timestamp)
 UPDATE eleve SET nom=$nom, prenom=$prenom, naissance=$naissance, sexe=$sexe, adresse=$adresse ,code_postal=$zipcode, commune=$commune, INE=$ine, classe=$classe, etab_id=:Établissement, niveau=:Niveau, enseignant_id=:Enseignant⋅e, comm_eleve=$comm_eleve, modification=$modif, editeur=$edition, suivi=$suivi WHERE id=$id and $prenom is not null
 returning
'redirect' AS component,
'eleves.sql?id='||$id||'' as link;

--Menu
SELECT 'dynamic' AS component, sqlpage.read_file_as_text('menu.json') AS properties;

--
-- Set a variable
SET suivi_edit = (SELECT suivi FROM eleve where id = $id);
SET nom_edit = (SELECT nom FROM eleve WHERE id = $id);
SET prenom_edit = (SELECT prenom FROM eleve WHERE id = $id);
SET naissance_edit = (SELECT naissance FROM eleve WHERE id = $id);

SET ine_edit = (SELECT INE FROM eleve WHERE id = $id);
SET sexe_edit = (SELECT sexe FROM eleve WHERE id = $id);
SET adresse_edit = (SELECT adresse FROM eleve WHERE id = $id);
SET zip_edit = (SELECT code_postal FROM eleve WHERE id = $id);
SET commune_edit = (SELECT commune FROM eleve WHERE id = $id);

SET etab_edit = (SELECT etab_id FROM eleve WHERE id = $id);
SET niv_edit = (SELECT niveau FROM eleve WHERE id = $id);
SET classe_edit = (SELECT classe FROM eleve WHERE id = $id);
SET enseignant_edit = (SELECT enseignant_id FROM eleve WHERE id = $id);
SET comm_edit = (SELECT comm_eleve FROM eleve WHERE id = $id);

--Bouton retour sans valider
select
    'button' as component,
    'sm'     as size,
    'pill'   as shape;
select
    'Retour à la liste' as title,
    'eleves.sql' as link,
    'arrow-back-up' as icon,
    'green' as outline;
select
    '+ Photo' as title,
    'camera-plus' as icon,
    'red' as outline,
    $group_bouton<4 as disabled,
    'eleve_photo_upload_form.sql?id='||$id as link FROM eleve where eleve.id=$id;
select
    'Parcours de l''élève' as title,
    'eleves_suivi.sql?id='|| $id as link,
    'list' as icon,
    'green' as outline;

-- écrire le nom de l'élève dans le titre de la page
SELECT
    'datagrid' as component,
    CASE WHEN EXISTS (SELECT eleve.id FROM image WHERE eleve.id=image.eleve_id)
  THEN image_url
  ELSE './icons/profil.png'
  END as image_url,
    UPPER(nom) || ' ' || prenom as title,
    'INE : '||INE as description
    FROM eleve LEFT JOIN image on image.eleve_id=eleve.id WHERE eleve.id = $id;
SELECT
    adresse||' - '||code_postal||' '||commune as title,
    'né(e) le : '||strftime('%d/%m/%Y',eleve.naissance)   as description, 'black' as color,
    0 as active
    FROM eleve LEFT JOIN image on image.eleve_id=eleve.id WHERE eleve.id = $id;
select
    etab.type||' '||etab.nom_etab as title,
    CASE WHEN classe<>''
    THEN 'Classe : ' || classe
    ELSE 'Niveau : ' || (SELECT niv FROM niveaux JOIN eleve on niveaux.niv=eleve.niveau)
    END as description,
    1 as active, 'green' as color,
    'etab_classes.sql?id='||etab.id||'&classe_select='||eleve.classe as link
    FROM eleve INNER JOIN etab on eleve.etab_id=etab.id WHERE eleve.id = $id;


    SELECT
    'form' as component,
    'Mettre à jour' as validate,
    'orange'           as validate_color;

    SELECT 'SUIVI' AS label, 'suivi' AS name, 'checkbox' as type, $suivi_edit=1 as checked, 1 as value, 1 as width;
    SELECT 'Nom' AS label, 'user' as prefix_icon, 'nom' AS name, $nom_edit as value, 4 as width, TRUE as required;
    SELECT 'Prénom' AS label, 'user' as prefix_icon, 'prenom' AS name, $prenom_edit as value, 4 as width, TRUE as required;
    SELECT 'Date de naissance' AS label, 'calendar-month' as prefix_icon, 'naissance' AS name, 'date' as type, $naissance_edit as value, 3 as width;
    SELECT 'Sexe' AS label, 'friends' as prefix_icon, 'sexe' AS name, $sexe_edit as value, 'select' as type, '[{"label": "F", "value": "F"}, {"label": "M", "value": "M"}]' as options, 1 as width;
    SELECT 'Adresse' AS label, 'address-book' as prefix_icon, 'adresse' AS name, 'text' as type, $adresse_edit as value, 6 as width;
    SELECT 'Code Postal' AS label, 'mail' as prefix_icon, 'zipcode' AS name, 'text' as type, $zip_edit as value, 2 as width;
    SELECT 'Commune' AS label, 'building-community' as prefix_icon, 'commune' AS name, 'text' as type, $commune_edit as value, 4 as width;
    SELECT 'INE' AS label, 'barcode' as prefix_icon, 'ine' AS name, 'text' as type, $ine_edit as value, 2 as width;
    SELECT 'Établissement' AS name, 3 as width,
          CAST($etab_edit as integer) as value,
    'select' as type, json_group_array(json_object("label", nom_etab, "value", id)) as options FROM (select nom_etab, id FROM etab union all
        select 'Aucun' as label, NULL as value
            ORDER BY nom_etab ASC);
    SELECT 'Niveau' AS name, 'select' as type, 2 as width, $niv_edit as value, json_group_array(json_object("label", niv, "value", niv)) as options FROM (select niv, ordre FROM niveaux union all
        select '-' as label, NULL as value ORDER BY ordre ASC);
    SELECT 'Classe' AS label, 'users-group' as prefix_icon, 'classe' AS name, $classe_edit as value, 2 as width;
    SELECT 'Enseignant⋅e' AS name, 'select' as type, 3 as width, CAST($enseignant_edit as integer) as value,
    json_group_array(json_object("label", nom_ens, "value", id)) as options FROM (select nom_ens, id FROM enseignant union all
        select 'Aucun' as label, NULL as value ORDER BY nom_ens ASC);
    SELECT 'Commentaire' AS label, 'comm_eleve' AS name, $comm_edit as value,'textarea' as type, 12 as width;
