SELECT 'redirect' AS component,
        'signin.sql?error' AS link
 WHERE NOT EXISTS (SELECT 1 FROM login_session WHERE id=sqlpage.cookie('session'));
SET group_id = (SELECT user_info.groupe FROM login_session join user_info on user_info.username=login_session.username WHERE id = sqlpage.cookie('session'));

SELECT 'redirect' AS component,
        'etablissement.sql?restriction' AS link
FROM eleve WHERE (SELECT user_info.etab FROM login_session join user_info on user_info.username=login_session.username WHERE id = sqlpage.cookie('session') and user_info.etab<>$id);

--Menu
SELECT 'dynamic' AS component, 
CASE WHEN $group_id=1
THEN sqlpage.read_file_as_text('index.json')
ELSE sqlpage.read_file_as_text('menu.json')
            END    AS properties; 

--Bouton retour à la liste des établissements
select 
    'button' as component,
    'sm'     as size,
    'pill'   as shape;
select 
    'Retour à la liste des établissements' as title,
    'etablissement.sql' as link,
    'arrow-back-up' as icon,
    'green' as outline;

--Boutons de navigation dans les données de l'établissement

select 
    'button' as component,
    'sm'     as size,
    --'pill'   as shape,
    'center' as justify;
select 
    'Classes' as title,
    'etab_classes.sql?id=' || $id as link,
    'users-group' as icon,
    'orange' as outline;
select 
    'Carte' as title,
    'map' as icon,
    'teal' as color;
select 
    'Stats' as title,
    'etab_stats.sql?id=' || $id as link,
    'chart-histogram' as icon,
    'teal' as outline;
select 
    'Photos' as title,
    'etab_trombi.sql?id=' || $id as link,
    'camera' as icon,
    'teal' as outline;


-- Récupère les nombres d'élèves dans 2 variables 
SET NB_eleve = (SELECT count(distinct eleve.id) FROM eleve where eleve.etab_id=$id);
SET NB_suivi = (SELECT count(distinct eleve.id) FROM eleve where eleve.etab_id=$id AND eleve.suivi=1);

-- écrire les infos de l'établissement dans le titre de la page [GRILLE]
SELECT 
    'datagrid' as component,
    type || ' ' || nom_etab as title FROM etab WHERE id = $id;
    select 
        ' Élèves suivi⋅es : ' as title,
        $NB_suivi ||' sur ' || $NB_eleve as description,
        'briefcase' as icon;

-- Carte 
SELECT 
    'map' as component,
    16 as zoom,
    Lat as latitude,
    Lon as longitude,
    400 as height
    FROM etab where etab.id=$id; 
SELECT
    type || ' ' || nom_etab as title,
    'red' as color,
    'building-community'        as icon,
    Lat AS latitude, 
    Lon AS longitude
FROM etab where etab.id=$id;      

